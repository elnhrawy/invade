package com.example.invade

import com.example.invade.core.di.DaggerCoreComponent
import com.example.invade.core.utils.ApplicationScopeProvider
import com.example.invade.core.utils.CoreComponentProvider
import android.app.Application
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.cancel
import timber.log.Timber

class InvadeApplication :
    Application(),
    CoreComponentProvider,
    ApplicationScopeProvider {
    private lateinit var coreComponent: com.example.invade.core.di.CoreComponent

    private val applicationScope = MainScope()

    override fun provideCoreComponent(): com.example.invade.core.di.CoreComponent = coreComponent


    override fun provideScope(): CoroutineScope = applicationScope

    override fun onLowMemory() {
        super.onLowMemory()
        applicationScope.cancel()
    }

    override fun onCreate() {
        super.onCreate()

        coreComponent = initCoreDependencyInjection()

        initTimber()
    }

    override fun onTerminate() {
        super.onTerminate()
    }

    private fun initCoreDependencyInjection(): com.example.invade.core.di.CoreComponent {
        return DaggerCoreComponent.builder()
            .contextModule(com.example.invade.core.di.modules.ContextModule(this))
            .build()
    }



    private fun initTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}
