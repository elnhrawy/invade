package com.example.invade.activity


sealed class MainActivityEffect {
    data class NavigateToDestination(
        val selectedBottomNavigationItemId: Int,
        val clearBackStack: Boolean = false,
    ) : MainActivityEffect()


}
