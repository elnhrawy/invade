package com.example.invade.activity.di

import com.example.invade.activity.MainActivity
import com.example.invade.core.di.scopes.FeatureScope
import dagger.Component

@FeatureScope
@Component(
    modules = [MainActivityModule::class],
    dependencies = [com.example.invade.core.di.CoreComponent::class],
)
interface MainActivityComponent {
    fun inject(activity: MainActivity)
}
