package com.example.invade.activity

import android.os.Bundle
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.example.invade.activity.di.DaggerMainActivityComponent
import com.example.invade.activity.di.MainActivityModule
import com.example.invade.commons.ui.R
import com.example.invade.commons.ui.extensions.coreComponent
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject


class MainActivity : AppCompatActivity() {
    @Inject
    lateinit var viewModel: MainActivityViewModel

    private val navController: NavController by lazy {
        (supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment).navController
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        injectDependencies()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        onBackPressedDispatcher.addCallback(backPressedCallback)
        consumeEffects(viewModel.effect)
    }


    private fun injectDependencies() {
        DaggerMainActivityComponent.builder()
            .coreComponent(coreComponent)
            .mainActivityModule(MainActivityModule(this))
            .build().inject(this)
    }

    private fun consumeEffects(effectFlow: Flow<MainActivityEffect>) {
        lifecycleScope.launch {
            effectFlow.collectLatest {
                handleEffect(it)
            }
        }
    }

    private fun handleEffect(effect: MainActivityEffect) {

    }


    private val backPressedCallback: OnBackPressedCallback =
        object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                moveTaskToBack(false)
            }
        }
}
