package com.example.invade.activity.di

import android.app.Application
import com.example.invade.activity.MainActivity
import com.example.invade.activity.MainActivityViewModel
import com.example.invade.commons.ui.extensions.viewModel
import com.example.invade.core.di.scopes.FeatureScope
import com.example.invade.core.local.database.DariDatabase
import com.example.invade.core.local.entity.SingleEvent
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.flow.Flow

@Module
class MainActivityModule(private val activity: MainActivity) {
    @FeatureScope
    @Provides
    fun provideViewModel(
        tokenExpirationFlow: Flow<SingleEvent<Long>>,
        db: DariDatabase,
        application: Application,
    ) = activity.viewModel {
        MainActivityViewModel(
            db,
            tokenExpirationFlow,
            application,
        )
    }
}
