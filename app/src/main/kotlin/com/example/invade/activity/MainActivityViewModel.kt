package com.example.invade.activity

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.invade.core.local.database.DariDatabase
import com.example.invade.core.local.database.delete
import com.example.invade.core.local.entity.SingleEvent
import com.example.invade.core.local.migrations.MigrationExceptionHandler
import com.example.invade.core.utils.CustomMutableSharedFlow
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.shareIn
import kotlinx.coroutines.launch

class MainActivityViewModel(
    private val db: DariDatabase,
    tokenExpirationFlow: Flow<SingleEvent<Long>>,
    private val application: Application,
) : ViewModel() {
    val effect: Flow<MainActivityEffect>
    private val _effect = CustomMutableSharedFlow<MainActivityEffect>()
    private val onApplyEffect: (MainActivityEffect) -> Unit = {
        viewModelScope.launch {
            _effect.emit(it)
        }
    }

    private val actions = CustomMutableSharedFlow<MainActivityAction>()
    private val applyAction: (MainActivityAction) -> Unit = {
        viewModelScope.launch {
            actions.emit(it)
        }
    }

    init {
        effect = _effect.shareIn(viewModelScope, SharingStarted.Eagerly)

        MigrationExceptionHandler.handleException {
            viewModelScope.launch {
                db.delete() // delete db then start with clean state
            }
        }


    }


}
