# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.kts.kts.kts.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile

-dontobfuscate

-keepattributes *Annotation*

-keepclassmembers class * {
    @com.google.gson.annotations.SerializedName <fields>;
}

-keepclassmembers enum * {
    public *;
}

-keepattributes JavascriptInterface
-keepattributes *Annotation*

-keepclassmembers class * {
   @android.webkit.JavascriptInterface <methods>;
}

# Please add these rules to your existing keep rules in order to suppress warnings.
# This is generated automatically by the Android Gradle plugin.
# R8
-dontwarn androidx.compose.runtime.collection.MutableVector
-dontwarn androidx.compose.ui.Modifier$Companion
-dontwarn androidx.compose.ui.Modifier$Element
-dontwarn androidx.compose.ui.Modifier
-dontwarn androidx.compose.ui.draw.DrawModifier
-dontwarn androidx.compose.ui.graphics.drawscope.ContentDrawScope
-dontwarn androidx.compose.ui.layout.MeasurePolicy
-dontwarn androidx.compose.ui.text.AnnotatedString
-dontwarn androidx.compose.ui.text.MultiParagraph
-dontwarn androidx.compose.ui.text.MultiParagraphIntrinsics
-dontwarn androidx.compose.ui.text.TextLayoutResult
-dontwarn androidx.compose.ui.text.TextRange
-dontwarn androidx.compose.ui.text.TextStyle
-dontwarn androidx.compose.ui.text.font.FontFamily$Resolver
-dontwarn androidx.compose.ui.text.input.TextFieldValue
-dontwarn androidx.compose.ui.unit.Constraints
-dontwarn androidx.compose.ui.unit.ConstraintsKt
-dontwarn androidx.compose.ui.unit.Density
-dontwarn com.bumptech.glide.Glide
-dontwarn com.bumptech.glide.RequestBuilder
-dontwarn com.bumptech.glide.RequestManager
-dontwarn com.bumptech.glide.request.BaseRequestOptions
-dontwarn com.bumptech.glide.request.RequestOptions
-dontwarn com.bumptech.glide.request.target.ViewTarget
-dontwarn com.google.android.exoplayer2.ExoPlayer$Builder
-dontwarn com.google.android.exoplayer2.ExoPlayer
-dontwarn com.google.android.exoplayer2.MediaItem
-dontwarn com.google.android.exoplayer2.Player$Listener
-dontwarn com.google.android.exoplayer2.Player
-dontwarn com.google.android.exoplayer2.source.MediaSource
-dontwarn com.google.android.exoplayer2.source.hls.HlsMediaSource$Factory
-dontwarn com.google.android.exoplayer2.source.hls.HlsMediaSource
-dontwarn com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection$Factory
-dontwarn com.google.android.exoplayer2.trackselection.DefaultTrackSelector
-dontwarn com.google.android.exoplayer2.trackselection.ExoTrackSelection$Factory
-dontwarn com.google.android.exoplayer2.trackselection.TrackSelector
-dontwarn com.google.android.exoplayer2.ui.StyledPlayerView
-dontwarn com.google.android.exoplayer2.upstream.BandwidthMeter
-dontwarn com.google.android.exoplayer2.upstream.DataSource$Factory
-dontwarn com.google.android.exoplayer2.upstream.DefaultBandwidthMeter$Builder
-dontwarn com.google.android.exoplayer2.upstream.DefaultBandwidthMeter
-dontwarn com.google.android.exoplayer2.upstream.DefaultDataSource$Factory
-dontwarn com.google.android.exoplayer2.upstream.DefaultHttpDataSource$Factory
-dontwarn com.google.android.exoplayer2.upstream.TransferListener
-dontwarn com.google.android.exoplayer2.util.Util
-dontwarn com.google.protobuf.java_com_google_android_gmscore_sdk_target_granule__proguard_group_gtm_N1281923064GeneratedExtensionRegistryLite$Loader

# Please add these rules to your existing keep rules in order to suppress warnings.
# This is generated automatically by the Android Gradle plugin.
-dontwarn androidx.preference.DialogPreference
-dontwarn androidx.preference.ListPreference
-dontwarn androidx.preference.MultiSelectListPreference
-dontwarn androidx.preference.Preference
-dontwarn androidx.preference.PreferenceGroupAdapter

# Ignore JSR 305 annotations for embedding nullability information.
-dontwarn javax.annotation.**

# Guarded by a NoClassDefFoundError try/catch and only used when on the classpath.
-dontwarn kotlin.Unit

#-----Lokalise------ Start
-keep class com.lokalise.** { *; }
-dontwarn com.lokalise.*
-keep @interface io.realm.annotations.RealmModule { *; }
-keep class io.realm.annotations.RealmModule { *; }

# Keep Realm classes
-keep class io.realm.** { *; }

# Keep classes that might be accessed via reflection by native code or Realm
-keep class * {
    @io.realm.annotations.RealmModule *;
}

-keep @io.realm.annotations.RealmModule class *

# Keep methods and fields that are accessed via JNI
-keepclassmembers class * {
    native <methods>;
}

# Keep enum values, which are used in the Realm's schema
-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

# Keep the constructors that are used by Realm
-keepclassmembers class * {
    public <init>(...);
}

# Retrofit does reflection on generic parameters. InnerClasses is required to use Signature and
# EnclosingMethod is required to use InnerClasses.
-keepattributes Signature, InnerClasses, EnclosingMethod

# Retrofit does reflection on method and parameter annotations.
-keepattributes RuntimeVisibleAnnotations, RuntimeVisibleParameterAnnotations

# Keep annotation default values (e.g., retrofit2.http.Field.encoded).
-keepattributes AnnotationDefault

# Retain service method parameters when optimizing.
-keepclassmembers,allowshrinking,allowobfuscation interface * {
    @retrofit2.http.* <methods>;
}

# Ignore annotation used for build tooling.
-dontwarn org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement

# Top-level functions that can only be used by Kotlin.
-dontwarn retrofit2.KotlinExtensions
-dontwarn retrofit2.KotlinExtensions$*

# With R8 full mode, it sees no subtypes of Retrofit interfaces since they are created with a Proxy
# and replaces all potential values with null. Explicitly keeping the interfaces prevents this.
-if interface * { @retrofit2.http.* <methods>; }
-keep,allowobfuscation interface <1>

# Keep inherited services.
-if interface * { @retrofit2.http.* <methods>; }
-keep,allowobfuscation interface * extends <1>

# With R8 full mode generic signatures are stripped for classes that are not
# kept. Suspend functions are wrapped in continuations where the type argument
# is used.
-keep,allowobfuscation,allowshrinking class kotlin.coroutines.Continuation

# R8 full mode strips generic signatures from return types if not kept.
-if interface * { @retrofit2.http.* public *** *(...); }
-keep,allowoptimization,allowshrinking,allowobfuscation class <3>

# With R8 full mode generic signatures are stripped for classes that are not kept.
-keep,allowobfuscation,allowshrinking class retrofit2.Response
#-----Lokalise------ End

# Please add these rules to your existing keep rules in order to suppress warnings.
# This is generated automatically by the Android Gradle plugin.
-dontwarn androidx.compose.foundation.gestures.Orientation
-dontwarn androidx.compose.runtime.internal.StabilityInferred
-dontwarn androidx.compose.ui.semantics.Role
-dontwarn androidx.compose.ui.semantics.SemanticsConfiguration
-dontwarn androidx.compose.ui.semantics.SemanticsConfigurationKt
-dontwarn androidx.compose.ui.semantics.SemanticsModifier
-dontwarn androidx.compose.ui.semantics.SemanticsProperties
-dontwarn androidx.compose.ui.semantics.SemanticsPropertyKey
-dontwarn androidx.compose.ui.state.ToggleableState
-dontwarn androidx.compose.ui.state.ToggleableStateKt
-dontwarn androidx.window.extensions.WindowExtensions
-dontwarn androidx.window.extensions.WindowExtensionsProvider
-dontwarn androidx.window.extensions.embedding.ActivityEmbeddingComponent
-dontwarn androidx.window.extensions.embedding.ActivityStack
-dontwarn androidx.window.extensions.embedding.SplitInfo
-dontwarn androidx.window.extensions.layout.DisplayFeature
-dontwarn androidx.window.extensions.layout.FoldingFeature
-dontwarn androidx.window.extensions.layout.WindowLayoutComponent
-dontwarn androidx.window.extensions.layout.WindowLayoutInfo
-dontwarn androidx.window.sidecar.SidecarDeviceState
-dontwarn androidx.window.sidecar.SidecarDisplayFeature
-dontwarn androidx.window.sidecar.SidecarInterface$SidecarCallback
-dontwarn androidx.window.sidecar.SidecarInterface
-dontwarn androidx.window.sidecar.SidecarProvider
-dontwarn androidx.window.sidecar.SidecarWindowLayoutInfo
-dontwarn com.android.billingclient.api.BillingClientStateListener
-dontwarn com.android.billingclient.api.PurchasesUpdatedListener
-dontwarn org.slf4j.Logger
-dontwarn org.slf4j.LoggerFactory
