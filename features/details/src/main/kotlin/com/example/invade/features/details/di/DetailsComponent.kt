package com.example.invade.features.details.di

import com.example.invade.core.di.CoreComponent
import com.example.invade.core.di.scopes.FeatureScope
import com.example.invade.features.details.FragmentDetails

import dagger.Component

@FeatureScope
@Component(
    modules = [DetailsModule::class],
    dependencies = [CoreComponent::class]
)
interface DetailsComponent {

    fun inject(fragment: FragmentDetails)
}
