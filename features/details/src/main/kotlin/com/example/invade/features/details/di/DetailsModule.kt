package com.example.invade.features.details.di

import androidx.navigation.fragment.navArgs
import com.example.invade.commons.ui.extensions.viewModel
import com.example.invade.commons.ui.resources.ResourcesLoader
import com.example.invade.core.di.scopes.FeatureScope
import com.example.invade.core.repos.home.HomeRepo
import com.example.invade.features.details.DetailsViewModel
import com.example.invade.features.details.FragmentDetails
import com.example.invade.features.details.FragmentDetailsArgs
import dagger.Module
import dagger.Provides

@Module
class DetailsModule(private val fragment: FragmentDetails) {

    @FeatureScope
    @Provides
    fun provideViewModel(
        homeRepo: HomeRepo,
    ) = fragment.viewModel {
        val args by fragment.navArgs<FragmentDetailsArgs>()

        DetailsViewModel(
            homeRepo,
            ResourcesLoader(fragment.requireContext()),
            args.universityID
        )
    }
}
