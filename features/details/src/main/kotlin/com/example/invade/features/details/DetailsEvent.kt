package com.example.invade.features.details

sealed class DetailsEvent {

    data object Dismiss : DetailsEvent()
    data object FetchData : DetailsEvent()

}
