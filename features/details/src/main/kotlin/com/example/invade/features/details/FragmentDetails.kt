package com.example.invade.features.details

import com.example.invade.commons.ui.base.BaseFragment
import com.example.invade.commons.ui.extensions.coreComponent
import com.example.invade.commons.ui.extensions.observe
import com.example.invade.features.details.databinding.FragmentDetailsBinding
import android.os.Bundle
import android.view.View
import com.example.invade.commons.navigation.onBackPressed
import com.example.invade.core.local.entity.home.UniversityEntity
import com.example.invade.features.details.di.DetailsModule
import com.example.invade.features.details.di.DaggerDetailsComponent

class FragmentDetails : BaseFragment<FragmentDetailsBinding, DetailsViewModel>(
    layoutId = R.layout.fragment_details
) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        observe(viewModel.event, this::handleEvent)
        observe(viewModel.universityItem, this::handleUI)
        initView()
    }

    fun initView(){
        viewBinding.refresh.setOnClickListener{
            viewModel.closeAndRefresh()
        }
    }
    override fun onInitDependencyInjection() {
        DaggerDetailsComponent.builder()
            .detailsModule(DetailsModule(this))
            .coreComponent(coreComponent)
            .build().inject(this)
    }

    override fun onInitDataBinding() {
        // no-op
    }

    private fun handleEvent(splashEvent: DetailsEvent) =
        when (splashEvent) {
            is DetailsEvent.Dismiss -> onBackPressed()
            else -> Unit
        }

    private fun handleUI(data: UniversityEntity) = with(viewBinding){
        nameTV.text = data.name
        stateTV.text = data.state
        countryTV.text = data.country
        countryCodeTV.text = data.code
        websiteTV.text = data.website
    }

}
