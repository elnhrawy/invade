package com.example.invade.features.details

import com.example.invade.commons.ui.livedata.SingleLiveData
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.invade.commons.ui.extensions.toSingleMediatorLiveData
import com.example.invade.commons.ui.resources.ResourcesLoader
import com.example.invade.core.local.entity.home.UniversityEntity
import com.example.invade.core.remote.Result
import com.example.invade.core.repos.home.HomeRepo

class DetailsViewModel(
    private val homeRepo: HomeRepo,
    private val resourcesLoader: ResourcesLoader,
    private val universityID: Long,

    ) : ViewModel() {


    private val _universityItem = MutableLiveData<UniversityEntity>()
    val universityItem: LiveData<UniversityEntity> = _universityItem


    private val _event = SingleLiveData<DetailsEvent>()
    val event: LiveData<DetailsEvent> = _event.toSingleMediatorLiveData { event, mediatorLiveData ->
        when (event) {

            is DetailsEvent.FetchData -> {
                val homeRequest = homeRepo.getUchUniversityById(universityID)
                mediatorLiveData.addSource(homeRequest) {
                  _universityItem.value = it
                }
                true
            }


            else -> false
        }
    }


    init {
        _event.value = DetailsEvent.FetchData

    }

    open fun closeAndRefresh(){
        _event.value = DetailsEvent.Dismiss

    }

}
