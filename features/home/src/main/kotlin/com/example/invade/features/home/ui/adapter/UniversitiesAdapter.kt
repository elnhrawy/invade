package com.example.invade.features.home.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.invade.commons.ui.base.BaseListAdapter
import com.example.invade.commons.ui.base.BaseViewHolder
import com.example.invade.core.local.entity.home.UniversityEntity
import com.example.invade.features.home.databinding.RowUniversityItemBinding

class UniversitiesAdapter(
    private val onUniversityClickListener: ((UniversityEntity) -> Unit)

) : BaseListAdapter<UniversityEntity>(
    itemsSame = { old, new -> old.id == new.id },
    contentsSame = { old, new -> old.name.contentEquals(new.name) && old.country.contentEquals(old.country) }
) {

    private var documentItems: List<UniversityEntity> = emptyList()

    override fun submitList(list: MutableList<UniversityEntity>?) {
        super.submitList(list)
        documentItems = list ?: emptyList()
    }

    override fun onCreateViewHolder(parent: ViewGroup, inflater: LayoutInflater, viewType: Int) = UniversityVH(parent, inflater, onUniversityClickListener)

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is UniversityVH) {
            holder.bind(documentItems[position])
        }
    }

    override fun getItemCount(): Int {
        return documentItems.size
    }

    class UniversityVH(
        parent: ViewGroup,
        inflater: LayoutInflater,
        private val onDocumentClickListener: (UniversityEntity) -> Unit,
    ) : BaseViewHolder<RowUniversityItemBinding>(
        RowUniversityItemBinding.inflate(inflater, parent, false)
    ) {

        fun bind(item: UniversityEntity) = binding.run {
            this.item = item

            container.setOnClickListener {
                onDocumentClickListener(item)
            }
        }
    }
}
