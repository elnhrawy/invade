package com.example.invade.features.home.ui



sealed class HomeListViewEvent {
    data class OpenUniversityDetails(val id: Long) : HomeListViewEvent()
    data object LoadHome : HomeListViewEvent()

}
