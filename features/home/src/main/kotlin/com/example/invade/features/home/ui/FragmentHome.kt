package com.example.invade.features.home.ui

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.invade.commons.navigation.navigate
import com.example.invade.commons.ui.R
import com.example.invade.commons.ui.base.BaseFragment
import com.example.invade.commons.ui.bindings.setItemDecorationSpacing
import com.example.invade.commons.ui.bindings.visible
import com.example.invade.commons.ui.extensions.coreComponent
import com.example.invade.commons.ui.extensions.observe
import com.example.invade.commons.views.popup.showError
import com.example.invade.core.local.entity.home.UniversityEntity
import com.example.invade.features.details.FragmentDetailsDirections
import com.example.invade.features.home.databinding.FragmentHomeBinding
import com.example.invade.features.home.di.DaggerHomeComponent
import com.example.invade.features.home.di.HomeModule
import com.example.invade.features.home.ui.adapter.UniversitiesAdapter
import com.example.invade.features.home.R as HR

class FragmentHome : BaseFragment<FragmentHomeBinding, HomeViewModel>(
    layoutId = HR.layout.fragment_home,
) {

    private var universitiesAdapter = UniversitiesAdapter {
        viewModel.openUniversityDetails(it.id)
    }

    override fun onViewCreated(
        view: View,
        savedInstanceState: Bundle?,
    ) {
        super.onViewCreated(view, savedInstanceState)

        initView()

        observe(viewModel.state, ::onViewStateChange)
        observe(viewModel.homeItemsLiveData, ::onViewDataChange)
        observe(viewModel.event, ::onViewEvent)

        viewModel.loadHome()
    }

    override fun onInitDataBinding() {
        viewBinding.viewModel = viewModel
    }

    override fun onInitDependencyInjection() {
        DaggerHomeComponent.builder()
            .homeModule(HomeModule(this))
            .coreComponent(coreComponent)
            .build().inject(this)
    }

    private fun initView() =
        viewBinding.apply {


            with(RVHome) {
                layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
                setHasFixedSize(true)
                val spacing = resources.getDimensionPixelSize(R.dimen._8sdp)
                setItemDecorationSpacing(0, spacing)
                adapter = universitiesAdapter
                itemAnimator = null
            }

        }


    private fun onViewStateChange(viewState: HomeListViewState) {
        val loadingView = viewBinding.shimmerViewContainer
        when (viewState) {
            is HomeListViewState.Loaded -> {
                loadingView.visible = false
                viewBinding.RVHome.visible = true
            }
            is HomeListViewState.Loading -> {
                loadingView.visible = true
                viewBinding.RVHome.visible = false
                viewBinding.emptyView.visible = false
            }
            is HomeListViewState.Error -> {
                loadingView.visible = false
                viewBinding.RVHome.visible = true
                showError(viewState.error)
            }
        }
    }


    private fun onViewEvent(event: HomeListViewEvent) {
        when (event) {
            is HomeListViewEvent.OpenUniversityDetails -> {
                navigate(FragmentDetailsDirections.openDetailsFlow(event.id))
            }

            else -> Unit
        }
    }

    private fun onViewDataChange(data: List<UniversityEntity>) {
        universitiesAdapter.submitList(data.toMutableList())
        viewBinding.emptyView.visible = data.isEmpty()
        viewBinding.RVHome.visible = data.isNotEmpty()

    }

}
