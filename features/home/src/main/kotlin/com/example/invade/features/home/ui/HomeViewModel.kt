package com.example.invade.features.home.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.invade.commons.ui.extensions.toSingleMediatorLiveData
import com.example.invade.commons.ui.resources.ResourcesLoader
import com.example.invade.core.local.entity.home.UniversityEntity
import com.example.invade.core.repos.home.HomeRepo
import com.example.invade.core.utils.cached
import com.example.invade.core.remote.Result
import androidx.lifecycle.LiveData

class HomeViewModel(
    private val homeRepo: HomeRepo,
    private val resourcesLoader: ResourcesLoader,
) : ViewModel() {

    private val _homeItemsLiveData = MutableLiveData<List<UniversityEntity>>()
    val homeItemsLiveData: LiveData<List<UniversityEntity>> = _homeItemsLiveData

    private val _state = cached<HomeListViewState>()
    val state: LiveData<HomeListViewState> = _state

    private val _event = cached<HomeListViewEvent>()
    val event: LiveData<HomeListViewEvent> =
        _event.toSingleMediatorLiveData { event, mediatorLiveData ->
            when (event) {

                is HomeListViewEvent.LoadHome -> {
                    val homeRequest = homeRepo.getHome()
                    mediatorLiveData.addSource(homeRequest) {
                        when (it) {
                            is Result.Success -> {
                                _homeItemsLiveData.value = it.data
                                _state.value = HomeListViewState.Loaded
                            }

                            is Result.Error -> _state.value = HomeListViewState.Error(it)
                            is Result.Loading -> _state.value = HomeListViewState.Loading
                        }
                    }
                    true
                }


                else -> false
            }
        }


     fun openUniversityDetails(id: Long) {
        _event.value = HomeListViewEvent.OpenUniversityDetails(id)
    }


     fun loadHome() {
        _event.value = HomeListViewEvent.LoadHome
    }


}
