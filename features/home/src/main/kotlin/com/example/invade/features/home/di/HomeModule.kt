package com.example.invade.features.home.di

import com.example.invade.commons.ui.extensions.viewModel
import com.example.invade.commons.ui.resources.ResourcesLoader
import com.example.invade.core.di.scopes.FeatureScope
import com.example.invade.core.repos.home.HomeRepo
import com.example.invade.features.home.ui.FragmentHome
import com.example.invade.features.home.ui.HomeViewModel
import dagger.Module
import dagger.Provides

@Module
class HomeModule(private val fragmentHome: FragmentHome) {
    @FeatureScope
    @Provides
    fun provideHomeViewModel(
        homeRepo: HomeRepo,
    ) = fragmentHome.viewModel {
        HomeViewModel(
            homeRepo,
            ResourcesLoader(fragmentHome.requireContext()),
        )
    }


}
