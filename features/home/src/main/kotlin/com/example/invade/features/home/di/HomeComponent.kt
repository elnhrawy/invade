package com.example.invade.features.home.di

import com.example.invade.core.di.scopes.FeatureScope
import com.example.invade.features.home.ui.FragmentHome
import dagger.Component

@FeatureScope
@Component(modules = [HomeModule::class], dependencies = [com.example.invade.core.di.CoreComponent::class])
interface HomeComponent {
    fun inject(home: FragmentHome)
}
