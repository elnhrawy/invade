package com.example.invade.features.home.ui

import com.example.invade.commons.ui.base.BaseViewState
import com.example.invade.core.remote.Result

sealed class HomeListViewState : BaseViewState {
    data object Loaded : HomeListViewState()

    data object Loading : HomeListViewState()

    data class Error(val error: Result.Error) : HomeListViewState()
}

