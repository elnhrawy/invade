import dependencies.Dependencies
import ext.implementation
plugins {
    id("common.android-dynamic-feature")
}
dependencies {
    implementation(project(BuildModules.Features.DETAILS))
}
android {
    namespace = "com.example.invade.features.home"
}
