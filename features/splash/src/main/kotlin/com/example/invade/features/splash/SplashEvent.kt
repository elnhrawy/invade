package com.example.invade.features.splash

sealed class SplashEvent {

    data object OpenHome : SplashEvent()

}
