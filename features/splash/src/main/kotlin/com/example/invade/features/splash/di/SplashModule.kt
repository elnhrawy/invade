package com.example.invade.features.splash.di

import com.example.invade.commons.ui.extensions.viewModel
import com.example.invade.core.di.scopes.FeatureScope
import com.example.invade.features.splash.FragmentSplash
import com.example.invade.features.splash.SplashViewModel
import dagger.Module
import dagger.Provides

@Module
class SplashModule(private val fragment: FragmentSplash) {

    @FeatureScope
    @Provides
    fun provideViewModel() = fragment.viewModel {
        SplashViewModel()
    }
}
