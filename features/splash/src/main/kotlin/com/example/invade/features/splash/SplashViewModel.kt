package com.example.invade.features.splash

import com.example.invade.commons.ui.livedata.SingleLiveData
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlin.math.max
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SplashViewModel : ViewModel() {

    private val _event = SingleLiveData<SplashEvent>()
    val event: LiveData<SplashEvent> = _event

    fun splashWait() {
        viewModelScope.launch(Dispatchers.Default) {

            delay(max(WAIT_MILLI, 0L))
            _event.postValue(SplashEvent.OpenHome)

        }
    }


    companion object {
        private const val WAIT_MILLI = 3000L
    }
}
