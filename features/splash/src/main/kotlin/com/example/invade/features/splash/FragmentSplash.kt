package com.example.invade.features.splash

import com.example.invade.commons.navigation.navigate
import com.example.invade.commons.ui.base.BaseFragment
import com.example.invade.commons.ui.extensions.coreComponent
import com.example.invade.commons.ui.extensions.observe
import com.example.invade.features.splash.databinding.FragmentSplashBinding
import com.example.invade.features.splash.di.SplashModule
import android.os.Bundle
import android.view.View
import com.example.invade.features.splash.di.DaggerSplashComponent

class FragmentSplash : BaseFragment<FragmentSplashBinding, SplashViewModel>(
    layoutId = R.layout.fragment_splash
) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        observe(viewModel.event, this::handleEvent)
        viewModel.splashWait()
    }

    override fun onInitDependencyInjection() {
        DaggerSplashComponent.builder()
            .splashModule(SplashModule(this))
            .coreComponent(coreComponent)
            .build().inject(this)
    }

    override fun onInitDataBinding() {
        // no-op
    }

    private fun handleEvent(splashEvent: SplashEvent) =
        when (splashEvent) {
            is SplashEvent.OpenHome -> navigate(FragmentSplashDirections.fromSplashToHome())
        }
}
