package com.example.invade.features.splash.di

import com.example.invade.core.di.CoreComponent
import com.example.invade.core.di.scopes.FeatureScope
import com.example.invade.features.splash.FragmentSplash
import dagger.Component

@FeatureScope
@Component(
    modules = [SplashModule::class],
    dependencies = [CoreComponent::class]
)
interface SplashComponent {

    fun inject(fragment: FragmentSplash)
}
