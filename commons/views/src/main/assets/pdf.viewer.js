
var url = location.search.substring(1);

function createPage() {
    var div = document.createElement("canvas");
    document.body.appendChild(div);
    return div;
}

function renderPage(num) {
    pdfDoc.getPage(num).then(function (page) {
        var canvas = createPage();

        var viewport = page.getViewport({ scale: 1.0 });
        // Support HiDPI-screens.
        var outputScale = window.devicePixelRatio || 1;

        canvas.width = Math.floor(viewport.width * outputScale);
        canvas.height = Math.floor(viewport.height * outputScale);
        canvas.style.width = Math.floor(viewport.width) + "px";
        canvas.style.height =  Math.floor(viewport.height) + "px";

        var transform = outputScale !== 1
          ? [outputScale, 0, 0, outputScale, 0, 0]
          : null;
        Android.isLoadingPDF(true)
        page.render({
            canvasContext: canvas.getContext('2d', {alpha: false}),
            viewport: viewport,
            transform: transform
        }).promise.then(function () {
                Android.isLoadingPDF(false)
            }
        );

      renderPage(++num);
       });

}

pdfjsLib.getDocument(url).promise.then(function (pdf) {
    pdfDoc = pdf;
    var i = 1;
    renderPage(i);
});

