package com.example.invade.commons.views.loading

import com.example.invade.commons.ui.R
import com.example.invade.commons.ui.extensions.getColorCompat
import android.content.Context
import android.graphics.PorterDuff
import android.util.AttributeSet
import android.widget.ProgressBar

class LoadingMoreFooterView
    @JvmOverloads
    constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = android.R.attr.progressBarStyleHorizontal,
    ) : ProgressBar(context, attrs, defStyleAttr) {
        init {
            indeterminateDrawable.setColorFilter(getContext().getColorCompat(R.color.dari_red), PorterDuff.Mode.SRC_IN)
            isIndeterminate = true
        }
    }
