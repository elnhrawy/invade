package com.example.invade.commons.views.emptyscreen

import com.example.invade.commons.ui.R
import com.example.invade.commons.ui.bindings.visible
import com.example.invade.commons.ui.databinding.EmptyScreenBinding
import com.example.invade.commons.ui.extensions.getStringValue
import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout

class EmptyView
    @JvmOverloads
    constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0,
    ) : ConstraintLayout(context, attrs, defStyleAttr) {
        private val binding = EmptyScreenBinding.inflate(LayoutInflater.from(context), this)

        var onCTAClicked: OnClickListener? = null
        var onContactUsClicked: OnClickListener? = null

        init {
            with(context.obtainStyledAttributes(attrs, R.styleable.EmptyView)) {
                binding.TVTitle.text = getStringValue(R.styleable.EmptyView_emptyTitle, context)
                binding.TVdesc.text = getStringValue(R.styleable.EmptyView_emptyDesc, context)
                binding.BtnProceed.text = getStringValue(R.styleable.EmptyView_emptyButtonTxt, context)
                binding.BtnProceed.visible = getBoolean(R.styleable.EmptyView_isShowButton, true)
                binding.IVImage.setImageResource(getResourceId(R.styleable.EmptyView_emptyImgDrawable, R.drawable.ic_empty_applications))
                getBoolean(R.styleable.EmptyView_isShowInfoView, false).let {
                    binding.infoViewGroup.visible = it
                }
                binding.BtnProceed.setOnClickListener { onCTAClicked?.onClick(it) }
                binding.TVContactUs.setOnClickListener { onContactUsClicked?.onClick(it) }
                recycle()
            }
        }

        // doesn't work with attr till i put method
        fun isShowInfoView(isShow: Boolean) {
            binding.infoViewGroup.visible = isShow
        }

        fun isShowButton(isShow: Boolean) {
            binding.BtnProceed.visible = isShow
        }

        fun emptyButtonTxt(txt: String) {
            binding.BtnProceed.text = txt
        }

        fun emptyDesc(txt: String) {
            binding.TVdesc.text = txt
        }
    }
