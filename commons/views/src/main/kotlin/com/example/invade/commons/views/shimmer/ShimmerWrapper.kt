package com.example.invade.commons.views.shimmer

import com.example.invade.commons.ui.R
import com.example.invade.commons.ui.databinding.ShimmerLayoutBinding
import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import com.facebook.shimmer.ShimmerFrameLayout

class ShimmerWrapper
    @JvmOverloads
    constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0,
    ) : ShimmerFrameLayout(context, attrs, defStyleAttr) {
        private val binding = ShimmerLayoutBinding.inflate(LayoutInflater.from(context), this)

        init {
            with(context.obtainStyledAttributes(attrs, R.styleable.ShimmerWrapper)) {
                repeat(getInt(R.styleable.ShimmerWrapper_count, 0)) {
                    binding.rootLinearLayout.addView(
                        LayoutInflater.from(context).inflate(
                            getResourceId(R.styleable.ShimmerWrapper_shimmerViewId, 0),
                            this@ShimmerWrapper,
                            false,
                        ),
                    )
                }
                startShimmer()
                recycle()
            }
        }
    }
