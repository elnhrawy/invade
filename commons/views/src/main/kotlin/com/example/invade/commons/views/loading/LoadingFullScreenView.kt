package com.example.invade.commons.views.loading

import com.example.invade.commons.ui.bindings.visible
import com.example.invade.commons.ui.databinding.WidgetFullScreenLoaderBinding
import android.animation.ValueAnimator
import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout

class LoadingFullScreenView
    @JvmOverloads
    constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0,
    ) : ConstraintLayout(context, attrs, defStyleAttr) {
    var title: String? = null
        set(value) {
            field = value
            setMessage(value)
            binding.TVMessage.visible = value.isNullOrBlank().not()
        }
        private val binding = WidgetFullScreenLoaderBinding.inflate(LayoutInflater.from(context), this)

        init {
            with(binding) {
                with(loadingAnimation) {
                    repeatCount = ValueAnimator.INFINITE
                }
            }
            visibility = visibility
        }

        override fun setVisibility(visibility: Int) {
            super.setVisibility(visibility)

            with(binding.loadingAnimation) {
                if (visibility == View.VISIBLE) {
                    resumeAnimation()
                } else {
                    pauseAnimation()
                }
            }
        }
    private fun setMessage(title: String?) =
        title?.takeIf { it.isNotBlank() }?.let {
            binding.TVMessage.text = it
        }
    }
