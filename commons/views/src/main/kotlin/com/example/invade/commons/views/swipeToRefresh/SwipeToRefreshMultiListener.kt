package com.example.invade.commons.views.swipeToRefresh

import android.content.Context
import android.util.AttributeSet
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout

class SwipeToRefreshMultiListener
    @JvmOverloads
    constructor(
        context: Context,
        attrs: AttributeSet? = null,
    ) : SwipeRefreshLayout(context, attrs) {
        init {
            setOnRefreshListener {
                listeners.forEach { it.onRefresh() }
            }
        }

        private val listeners: MutableList<OnRefreshListener> = mutableListOf()

        fun addOnRefreshListener(listener: OnRefreshListener) {
            listeners.add(listener)
        }

        fun removeOnRefreshListener(listener: OnRefreshListener) {
            listeners.remove(listener)
        }
    }
