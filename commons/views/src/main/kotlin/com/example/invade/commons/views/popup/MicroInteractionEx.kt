package com.example.invade.commons.views.popup

import com.example.invade.commons.ui.R
import com.example.invade.commons.ui.databinding.WidgetMicroInteractionBinding
import com.example.invade.commons.ui.extensions.getScreenDimensions
import com.example.invade.commons.ui.extensions.getStringByStringResourceName
import com.example.invade.core.remote.Result
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.view.Gravity
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.FrameLayout
import android.widget.PopupWindow
import androidx.annotation.StringRes
import androidx.core.view.doOnPreDraw
import androidx.fragment.app.Fragment

private const val MICRO_DISMISS_AFTER_SEC = 3

fun Fragment.showError(
    @StringRes msg: Int,
) {
    showError(resources.getString(msg))
}

fun Fragment.showError(msg: String?) {
    showMicroInteraction(MicroInteractionType.FAILURE, msg ?: "")
}

fun Fragment.showError(error: Result.Error) {
    showError(error.errorCode, error.errorMessage, error.moreInfo, error.errorCause)
}

@JvmOverloads
fun Fragment.showError(
    errorCode: Long,
    errorMsg: String?,
    moreInfo: String? = null,
    errorCause: Throwable? = null,
) {
    showMicroInteraction(MicroInteractionType.FAILURE, getErrorMessage(errorCode, errorMsg, moreInfo, errorCause))
}

@JvmOverloads
fun Fragment.getErrorMessage(
    errorCode: Long,
    errorMsg: String?,
    moreInfo: String? = null,
    errorCause: Throwable? = null,
): String =
    moreInfo?.let {
        val s =
            context?.getStringByStringResourceName(
                resourceName = "error_$errorCode",
                default = "",
            )
        try {
            s?.format(it)
        } catch (e: java.lang.Exception) {
            s
        }
    } ?: context?.getStringByStringResourceName(
        resourceName = "error_$errorCode",
        default = "",
    )?.takeIf { it.isNotBlank() }
        ?: errorMsg
        ?: errorCause?.localizedMessage
        ?: ""

fun Fragment.showSuccess(
    @StringRes msg: Int,
) {
    showSuccess(resources.getString(msg))
}

fun Fragment.showSuccess(msg: String) {
    showMicroInteraction(MicroInteractionType.SUCCESS, msg)
}

fun Fragment.showInfo(
    @StringRes msg: Int,
) {
    showInfo(resources.getString(msg))
}

fun Fragment.showInfo(msg: String) {
    showMicroInteraction(MicroInteractionType.INFO, msg)
}

fun Fragment.showMicroInteraction(
    type: MicroInteractionType,
    msg: String,
    dismissAfterSec: Int = MICRO_DISMISS_AFTER_SEC,
) {
    if (isDetached || context == null) {
        return
    }

    activity?.showMicroInteraction(type, msg, dismissAfterSec)
}

@SuppressLint("ClickableViewAccessibility")
fun Activity.showMicroInteraction(
    type: MicroInteractionType,
    msg: String,
    dismissAfterSec: Int = MICRO_DISMISS_AFTER_SEC,
) {
    var popUp: PopupWindow? = null

    val contentView =
        WidgetMicroInteractionBinding.inflate(LayoutInflater.from(this), null, false)
            .apply {
                val (bg, icon) =
                    when (type) {
                        MicroInteractionType.SUCCESS -> R.drawable.bg_micro_success to R.drawable.ic_micro_success
                        MicroInteractionType.FAILURE -> R.drawable.bg_micro_failure to R.drawable.ic_micro_error
                        MicroInteractionType.INFO -> R.drawable.bg_micro_info to R.drawable.ic_micro_info
                    }

                TVMsg.text = msg
                ImgIcon.setImageResource(icon)
                root.setBackgroundResource(bg)

                if (dismissAfterSec > 0) {
                    root.postDelayed(
                        {
                            popUp?.dismiss()
                        },
                        dismissAfterSec * 1000L,
                    )
                }
            }.root

    val marginTop = resources.getDimensionPixelSize(R.dimen._12sdp)
    val margin = resources.getDimensionPixelSize(R.dimen._12sdp)
    val screenDimen = getScreenDimensions()
    val popupWidth = screenDimen.x.minus(margin * 2)

    popUp =
        PopupWindow(
            contentView,
            popupWidth,
            ViewGroup.LayoutParams.WRAP_CONTENT,
            true,
        ).apply {
            showAtLocation(contentView, Gravity.NO_GRAVITY, margin, marginTop)

            setTouchInterceptor { _: View, event: MotionEvent ->
                if (event.action == MotionEvent.ACTION_UP) {
                    dismiss()
                }
                dispatchTouchEvent(event)
                true
            }
        }

    contentView.doOnPreDraw {
        it.safelyUpdateLayoutParams<FrameLayout.LayoutParams> {
            setMargins(0, marginTop, 0, 0)
        }
    }
}

fun PopupWindow.dimBehind(dimFraction: Float = 0.3f) {
    val container = contentView.rootView
    val context = contentView.context
    val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
    val lp = container.layoutParams as WindowManager.LayoutParams
    lp.flags = lp.flags or WindowManager.LayoutParams.FLAG_DIM_BEHIND
    lp.dimAmount = dimFraction
    wm.updateViewLayout(container, lp)
}

private inline fun <reified T : ViewGroup.LayoutParams> View.safelyUpdateLayoutParams(block: T.() -> Unit) {
    val params = layoutParams as? T
    params?.let {
        block(it)
        layoutParams = it
    }
}

enum class MicroInteractionType {
    SUCCESS,
    FAILURE,
    INFO,
}
