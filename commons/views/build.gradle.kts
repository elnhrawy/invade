import dependencies.Dependencies
import ext.implementation

plugins {
    id("common.android-library")
    id(BuildPlugins.NAVIGATION_SAFE_ARGS)
}

dependencies {
    implementation(project(BuildModules.Commons.UI))
    implementation(project(BuildModules.CORE))
    implementation(project(BuildModules.Commons.NAVIGATION))
    implementation(Dependencies.CONSTRAIN_LAYOUT)
    implementation(Dependencies.MATERIAL)
    implementation(Dependencies.RECYCLE_VIEW)
    implementation(Dependencies.CORE_KTX)
    implementation(Dependencies.LOTTIE)
    implementation(Dependencies.NAVIGATION_FRAGMENT)
    implementation(Dependencies.SWIPE_REFRESH_LAYOUT)
    api(Dependencies.SHIMMER)
}
android {
    namespace = "com.example.invade.commons.views"
}
