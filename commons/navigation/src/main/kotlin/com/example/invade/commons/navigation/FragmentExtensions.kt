package com.example.invade.commons.navigation

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController

fun Fragment.navigate(direction: NavDirections) {
    safeNavigate(direction)
}

fun Fragment.popBackStack() {
    findNavController().popBackStack()
}

fun Fragment.onBackPressed() {
    requireActivity().onBackPressed()
}

fun Fragment.previousSavedStateHandle() = previousBackStackEntry()?.savedStateHandle

fun Fragment.currentSavedStateHandle() = currentBackStackEntry()?.savedStateHandle

fun Fragment.currentBackStackEntry() = findNavController().currentBackStackEntry

fun Fragment.previousBackStackEntry() = findNavController().previousBackStackEntry

fun Fragment.openAppInGooglePlay() {
    val activity = requireActivity()
    activity.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=${activity.packageName}")))
}

fun Fragment.sendEmail(email: String) {
    val intent = Intent(Intent.ACTION_SENDTO)
    intent.data = Uri.parse("mailto:")
    intent.putExtra(Intent.EXTRA_EMAIL, arrayOf(email))
    startActivitySafe(intent)
}

@Suppress("MagicNumber")
fun Fragment.openMap(
    lat: Double,
    lng: Double,
    zoom: Int = 17,
) {
    val intent = Intent(Intent.ACTION_VIEW, Uri.parse("geo:$lat,$lng?q=$lat,$lng&z=$zoom"))
    intent.setPackage("com.google.android.apps.maps")
    startActivitySafe(intent)
}

fun Fragment.makeDial(phone: String) {
    val intent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null))
    startActivitySafe(intent)
}

fun Fragment.shareText(content: String) {
    val intent =
        Intent(Intent.ACTION_SEND).apply {
            type = "text/plain"
            putExtra(Intent.EXTRA_TEXT, content)
        }
    startActivitySafe(intent)
}

fun Fragment.openWebBrowser(url: String) {
    val uri = Uri.parse("googlechrome://navigate?url=$url")
    val intent = Intent(Intent.ACTION_VIEW, uri)
    startActivitySafe(
        intent = intent,
        onError = {
            intent.data = Uri.parse(url)
            startActivitySafe(intent)
        },
    )
}

fun Fragment.openWhatsApp(url: String) {
    val intent = Intent(Intent.ACTION_VIEW)
    intent.data = Uri.parse(url)
    startActivitySafe(
        intent = intent
    )
}

private fun Fragment.startActivitySafe(
    intent: Intent,
    onError: (() -> Unit)? = null,
) = try {
    startActivity(intent)
} catch (e: ActivityNotFoundException) {
    onError?.invoke() ?: Toast.makeText(requireContext(), e.localizedMessage, Toast.LENGTH_SHORT).show()
}

private fun Fragment.safeNavigate(direction: NavDirections) {
    with(findNavController()) {
        val actionId = direction.actionId
        val action = currentDestination?.getAction(actionId) ?: graph.getAction(actionId)
        if (action != null) {
            navigate(direction)
        }
    }
}
