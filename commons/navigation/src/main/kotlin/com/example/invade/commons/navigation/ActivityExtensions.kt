package com.example.invade.commons.navigation

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.widget.Toast

fun Activity.openMaps(
    lat: Double,
    lng: Double,
    zoom: Int,
) {
    val intent = Intent(Intent.ACTION_VIEW, Uri.parse("geo:$lat,$lng?q=$lat,$lng&z=$zoom"))
    intent.setPackage("com.google.android.apps.maps")
    startActivitySafe(intent)
}

private fun Activity.startActivitySafe(
    intent: Intent,
    onError: (() -> Unit)? = null,
) = try {
    startActivity(intent)
} catch (e: ActivityNotFoundException) {
    onError?.invoke() ?: Toast.makeText(this, e.localizedMessage, Toast.LENGTH_SHORT).show()
}
