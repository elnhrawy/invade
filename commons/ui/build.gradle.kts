import dependencies.Dependencies
import ext.implementation

plugins {
    id("common.android-library")
}

android {
    defaultConfig {
        buildConfigField("boolean", "isResizeEnabled", BuildAndroidConfig.IS_RESIZE_ENABLED)
    }
    namespace = "com.example.invade.commons.ui"
    buildFeatures {
        buildConfig = true
    }
}

dependencies {
    implementation(project(BuildModules.CORE))
    implementation(project(BuildModules.Commons.NAVIGATION))
    implementation(Dependencies.LIFECYCLE_VIEWMODEL)
    implementation(Dependencies.RECYCLE_VIEW)
    implementation(Dependencies.MATERIAL)
    implementation(Dependencies.CORE_KTX)
    implementation(Dependencies.FRAGMENT_KTX)
    implementation(Dependencies.COIL)
    implementation(Dependencies.NAVIGATION_FRAGMENT)
    implementation(Dependencies.LOTTIE)

}
