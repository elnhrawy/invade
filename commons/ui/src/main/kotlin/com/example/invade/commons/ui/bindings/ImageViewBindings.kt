package com.example.invade.commons.ui.bindings

import com.example.invade.commons.ui.BuildConfig
import com.example.invade.commons.ui.R
import com.example.invade.commons.ui.extensions.getColorCompat
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.view.ViewTreeObserver
import android.widget.ImageView
import androidx.annotation.DrawableRes
import androidx.databinding.BindingAdapter
import coil.Coil
import coil.load
import coil.request.ImageRequest
import coil.target.Target
import coil.transform.CircleCropTransformation

private const val THUMBOR_URL = "https://thumbor.dari.ae/unsafe/"

@BindingAdapter("imageUrl", "imagePlaceholder", "imageFail", "isCircle", requireAll = false)
fun ImageView.imageUrl(
    url: String?,
    placeholder: Drawable? = null,
    fallback: Drawable? = null,
    isCircle: Boolean = false,
) {
    fun loadImg(imgUrl: String?) {
        load(imgUrl) {
            val defaultPlaceholder = ColorDrawable(context.getColorCompat(R.color.image_placeholder))
            allowHardware(false)

            crossfade(true)

            placeholder(placeholder ?: defaultPlaceholder)

            fallback(fallback ?: placeholder ?: defaultPlaceholder)

            error(fallback ?: placeholder ?: defaultPlaceholder)

            if (isCircle) {
                transformations(CircleCropTransformation())
            }
        }
    }
    if (BuildConfig.isResizeEnabled && url?.isNotBlank() == true) {
        viewTreeObserver.addOnGlobalLayoutListener(
            object : ViewTreeObserver.OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    viewTreeObserver.removeOnGlobalLayoutListener(this)
                    val width = this@imageUrl.width
                    loadImg(if (width > 0) "$THUMBOR_URL${width}x0/$url" else url)
                }
            },
        )
    } else {
        loadImg(url)
    }
}

// to handle string resource like R.drawable.ic_icon
@BindingAdapter("loadIntResource")
fun ImageView.loadIntResource(
    @DrawableRes res: Int,
) {
    setImageResource(res)
}

@BindingAdapter("tintColorResource")
fun ImageView.tintColor(color: Int) {
    setColorFilter(color)
}

fun ImageView.loadImageAsBitmap(
    url: String,
    onLoaded: (Bitmap?) -> Unit,
) {
    val request =
        ImageRequest.Builder(context)
            .data(url)
            .allowHardware(false)
            .target(
                object : Target {
                    override fun onSuccess(result: Drawable) {
                        super.onSuccess(result)
                        onLoaded((result as? BitmapDrawable)?.bitmap)
                    }
                },
            ).build()
    Coil.imageLoader(context).enqueue(request)
}
