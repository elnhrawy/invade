package com.example.invade.commons.ui.extensions

import android.content.Context
import android.content.res.TypedArray
import androidx.annotation.StyleableRes

fun TypedArray.getStringValue(
    @StyleableRes res: Int,
    context: Context,
): String? =
    getResourceId(res, 0).takeIf { it != 0 }?.let {
        context.getString(it)
    }
