package com.example.invade.commons.ui.extensions

import android.content.Context
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStoreOwner
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.NavHostFragment
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@Suppress("UNCHECKED_CAST")
inline fun <reified VM : ViewModel> ViewModelStoreOwner.viewModel(crossinline factory: () -> VM): VM {
    val viewModelProviderFactory =
        object : ViewModelProvider.Factory {
            override fun <T : ViewModel> create(modelClass: Class<T>): T {
                return factory() as T
            }
        }

    return ViewModelProvider(this, viewModelProviderFactory)[VM::class.java]
}

fun Fragment.showKeyboard() {
    requireContext().getSystemService(Context.INPUT_METHOD_SERVICE).let {
        it as InputMethodManager
    }.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
}

val Fragment.coreComponent: com.example.invade.core.di.CoreComponent
    get() = requireActivity().coreComponent

val Fragment.actualParentFragment: Fragment?
    get() {
        var parent: Fragment? = parentFragment
        while (parent != null && parent is NavHostFragment) {
            parent = parent.parentFragment
        }
        return parent
    }

fun <T> Fragment.collectWhenStarted(
    flow: Flow<T>,
    collectFun: suspend (T) -> Unit,
) {
    viewLifecycleOwner.collectWhenStarted(flow, collectFun)
}

fun <T> Fragment.collectLatestWhenStarted(
    flow: Flow<T>,
    collectFun: suspend (T) -> Unit,
) {
    viewLifecycleOwner.collectLatestWhenStarted(flow, collectFun)
}

fun <T> LifecycleOwner.collectLatestWhenStarted(
    flow: Flow<T>,
    collectFun: suspend (T) -> Unit,
) {
    lifecycleScope.launch {
        flow.flowWithLifecycle(lifecycle, Lifecycle.State.STARTED).collectLatest(collectFun)
    }
}

fun <T> LifecycleOwner.collectWhenStarted(
    flow: Flow<T>,
    collectFun: suspend (T) -> Unit,
) {
    lifecycleScope.launch {
        flow.flowWithLifecycle(lifecycle, Lifecycle.State.STARTED).collect(collectFun)
    }
}
