package com.example.invade.commons.ui.resources

import com.example.invade.commons.ui.R
import com.example.invade.commons.ui.extensions.getStringByStringResourceName
import com.example.invade.commons.ui.extensions.getStringByStringResourceNameOrNull
import com.example.invade.commons.ui.extensions.getValidationErrorContent
import com.example.invade.commons.ui.extensions.getValidationErrorTitle
import com.example.invade.commons.ui.extensions.isAr
import android.content.Context
import android.content.res.Resources
import androidx.annotation.ArrayRes
import androidx.annotation.PluralsRes
import androidx.annotation.StringRes
import java.lang.ref.WeakReference

/**
 * Class to load resource using a weak reference to [Context], mainly used by [androidx.lifecycle.ViewModel]
 **/
class ResourcesLoader(context: Context) {
    private val contextWeakReference = WeakReference(context)

    private val context: Context?
        get() = contextWeakReference.get()

    private val resources: Resources?
        get() = context?.resources

    /**
     * see documentation for [LocalizationManager.unwrapLocalizationContext]
     * **/
//    private val localResources: Resources? by lazy {
//        contextWeakReference.get()?.let {
//            LocalizationManager.unwrapLocalizationContext(it)?.resources
//        }
//    }

    val isAr: Boolean by lazy {
        context.isAr
    }



    fun getString(
        @StringRes resId: Int,
    ): String? = resources?.getString(resId)

    fun getString(
        @StringRes resId: Int,
        vararg formatArg: Any,
    ): String? = resources?.getString(resId, *formatArg)

    fun getStringOrDefault(
        @StringRes resId: Int,
        default: String = "",
    ): String = getString(resId) ?: default

    fun getStringArray(
        @ArrayRes resId: Int,
    ): Array<String> = resources?.getStringArray(resId) ?: emptyArray()

    fun getQuantityString(
        @PluralsRes resId: Int,
        quantity: Int,
        vararg formatArg: Any,
    ): String? = resources?.getQuantityString(resId, quantity, *formatArg)

    fun getStringOrDefault(
        resourceName: String?,
        default: String? = resourceName,
    ): String =
        resourceName?.let {
            context?.getStringByStringResourceName(resourceName, default) ?: default
        } ?: default ?: ""




    fun getValidationErrorTitle(validationCode: String) = context?.getValidationErrorTitle(validationCode)

    fun getValidationErrorContent(validationCode: String) = context?.getValidationErrorContent(validationCode)

    fun getStringByResourceNameOrNull(resourceName: String): String? = context?.getStringByStringResourceNameOrNull(resourceName)

}
