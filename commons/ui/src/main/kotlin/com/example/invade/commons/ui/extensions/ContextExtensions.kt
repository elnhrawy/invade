package com.example.invade.commons.ui.extensions

import com.example.invade.core.utils.isOnlyDigits
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.graphics.Point
import android.graphics.Rect
import android.util.DisplayMetrics
import android.view.WindowManager
import android.widget.Toast
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.annotation.RawRes
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import androidx.core.os.ConfigurationCompat
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.Locale

fun Context.getColorCompat(
    @ColorRes color: Int,
) = ContextCompat.getColor(this, color)

fun Context.getDrawableCompat(
    @DrawableRes drawable: Int,
) = ContextCompat.getDrawable(this, drawable)

suspend fun Context.readRawRes(
    @RawRes raw: Int,
) = withContext(Dispatchers.IO) {
    resources.openRawResource(raw).bufferedReader().use {
        it.readText()
    }
}

fun Context.getScreenDimensions(): Point {
    return (this as? Activity)?.screenDimensions ?: run {
        val displayMetrics = DisplayMetrics()
        (getSystemService(Context.WINDOW_SERVICE) as WindowManager).defaultDisplay.getMetrics(displayMetrics).run {
            Point(displayMetrics.widthPixels, displayMetrics.heightPixels)
        }
    }
}

fun Context.getStatusBarHeight(): Int {
    val rect = Rect()
    (this as? Activity)?.window?.decorView?.getWindowVisibleDisplayFrame(rect)
    return rect.top
}

fun Context.showToast(
    @StringRes msgRes: Int,
) {
    showToast(getString(msgRes))
}

fun Context.showToast(msg: String) {
    Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
}

fun Context.getResourceByName(resourceName: String?): Int {
    if (resourceName == null) return -1
    return resources
        .getIdentifier(resourceName.lowercase(Locale.ROOT), "drawable", packageName)
}

fun Context.getStyleByName(resourceName: String?): Int {
    if (resourceName == null) return -1
    return try {
        resources
            .getIdentifier(resourceName, "style", packageName)
    } catch (_: Exception) {
        -1
    }
}

fun Context.getValidationErrorTitle(validationCode: String): String? = getStringByStringResourceNameOrNull("${validationCode.lowercase()}_title")

fun Context.getValidationErrorContent(validationCode: String): String? = getStringByStringResourceNameOrNull("${validationCode.lowercase()}_content")

fun Context.getErrorStringByErrorCode(
    errorCode: String,
    default: String? = null,
): String = getStringByStringResourceName("error_$errorCode", default ?: errorCode)

fun Context.getStringByStringResourceName(
    resourceName: String?,
    default: String? = null,
): String {
    val finalDefault = default ?: resourceName ?: ""
    if (resourceName.isNullOrBlank() || resourceName.isOnlyDigits()) return finalDefault

    return getStringByStringResourceNameOrNull(resourceName) ?: finalDefault
}

@SuppressLint("DiscouragedApi")
fun Context.getStringByStringResourceNameOrNull(resourceName: String): String? {
    val resourceId = resources.getIdentifier(resourceName, "string", packageName)
    return if (resourceId == 0) {
        null
    } else {
        try {
            resources.getString(resourceId)
        } catch (_: Throwable) {
            null
        }
    }
}

val Context.isAr: Boolean
    get() =
        resources?.let {
            ConfigurationCompat.getLocales(it.configuration)
                .get(0)?.language?.contains("ar")
        } ?: false

val Context.languageKey: String
    get() = if (isAr) "ar" else "en"

infix fun Boolean.ifArabic(ar: String?): String? = if (this && ar.isNullOrBlank().not()) ar else null

infix fun Context.ifArabic(ar: String?): String? = isAr ifArabic ar

infix fun String?.then(other: String?): String = this ?: other ?: ""

