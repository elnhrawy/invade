package com.example.invade.commons.ui.extensions

fun <T> MutableList<T>.addIfNotContains(t: T) {
    if (contains(t).not()) {
        add(t)
    }
}

fun <T> MutableList<T>.addIfNotContains(t: List<T>?) {
    t?.forEach {
        addIfNotContains(it)
    }
}
