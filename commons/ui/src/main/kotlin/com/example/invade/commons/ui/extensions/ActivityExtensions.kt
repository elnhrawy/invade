package com.example.invade.commons.ui.extensions

import com.example.invade.core.utils.CoreComponentProvider
import android.app.Activity
import android.content.Intent
import android.graphics.Point
import android.net.Uri
import android.os.Build
import android.util.DisplayMetrics

val Activity.screenDimensions: Point
    get() =
        Point().apply {
            val metrics = displayMetrics
            x = metrics.widthPixels
            y = metrics.heightPixels
        }

val Activity.displayMetrics: DisplayMetrics
    get() {
        val displayMetrics = DisplayMetrics()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            display?.getRealMetrics(displayMetrics)
        } else {
            windowManager?.defaultDisplay?.getMetrics(displayMetrics)
        }
        return displayMetrics
    }

val Activity.coreComponent: com.example.invade.core.di.CoreComponent
    get() = (application as CoreComponentProvider).provideCoreComponent()

fun Activity.restart(destination: Uri? = null) {
    val className = this::class.qualifiedName ?: return
    finish()
    startActivity(Intent().setData(destination).setClassName(packageName, className))
}
