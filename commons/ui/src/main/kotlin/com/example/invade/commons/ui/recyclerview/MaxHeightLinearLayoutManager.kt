package com.example.invade.commons.ui.recyclerview

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MaxHeightLinearLayoutManager(context: Context) : LinearLayoutManager(context, HORIZONTAL, false) {
    override fun onMeasure(
        recycler: RecyclerView.Recycler,
        state: RecyclerView.State,
        widthSpec: Int,
        heightSpec: Int,
    ) {
        super.onMeasure(recycler, state, widthSpec, heightSpec)
        var maxHeight = 0
        for (i in 0 until childCount) {
            val childHeight = getChildAt(i)?.measuredHeight ?: 0
            if (childHeight > maxHeight) {
                maxHeight = childHeight
            }
        }
        setMeasuredDimension(View.MeasureSpec.getSize(widthSpec), maxHeight)
    }
}
