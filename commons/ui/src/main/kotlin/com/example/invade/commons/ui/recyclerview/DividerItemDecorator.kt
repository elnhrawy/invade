package com.example.invade.commons.ui.recyclerview

import android.graphics.Canvas
import android.graphics.drawable.Drawable
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class DividerItemDecorator(
    private val mDivider: Drawable?,
    private val step: Int = 1,
    private val extraPaddingVertical: Int = 0,
    private val extraPaddingHorizontal: Int = 0,
) : RecyclerView.ItemDecoration() {
    override fun onDrawOver(
        canvas: Canvas,
        parent: RecyclerView,
        state: RecyclerView.State,
    ) {
        mDivider?.let {
            val dividerLeft: Int = parent.paddingLeft + extraPaddingHorizontal
            val dividerRight: Int = parent.width - parent.paddingRight - extraPaddingHorizontal
            val childCount: Int = parent.childCount
            val bound = step - 1
            for (i in bound until childCount - bound step step) {
                val child: View = parent.getChildAt(i)
                val params: RecyclerView.LayoutParams = child.layoutParams as RecyclerView.LayoutParams
                val dividerTop: Int = child.bottom + params.bottomMargin + extraPaddingVertical
                val dividerBottom: Int = dividerTop + mDivider.intrinsicHeight
                mDivider.setBounds(dividerLeft, dividerTop, dividerRight, dividerBottom)
                mDivider.draw(canvas)
            }
        }
    }
}
