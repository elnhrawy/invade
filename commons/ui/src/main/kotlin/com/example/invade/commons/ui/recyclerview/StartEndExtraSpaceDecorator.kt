package com.example.invade.commons.ui.recyclerview

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class StartEndExtraSpaceDecorator(private val spacingPx: Int) : RecyclerView.ItemDecoration() {
    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State,
    ) {
        val itemCount = state.itemCount

        when (parent.getChildAdapterPosition(view)) {
            0 -> outRect.left = spacingPx
            itemCount - 1 -> outRect.right = spacingPx
            else -> return
        }
    }
}
