package com.example.invade.commons.ui.extensions

import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SnapHelper

fun RecyclerView.attachPagerSnapHelper() = attachSnapHelper(PagerSnapHelper())

fun RecyclerView.attachSnapHelper(snapHelper: SnapHelper) {
    snapHelper.attachToRecyclerView(this)
}

/**
 * default configs for most of the [RecyclerView]s used.
 *
 *
 * `isNestedScrollingEnabled = false`
 *
 * `itemAnimator = null`
 */
fun RecyclerView.defaultConfigs() {
    isNestedScrollingEnabled = false
    itemAnimator = null
}
