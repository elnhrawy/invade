package com.example.invade.commons.ui.bindings

import com.example.invade.commons.ui.recyclerview.DividerItemDecorator
import com.example.invade.commons.ui.recyclerview.RecyclerViewHorizontalSpaceItemDecoration
import com.example.invade.commons.ui.recyclerview.RecyclerViewSpaceItemDecoration
import com.example.invade.commons.ui.recyclerview.StartEndExtraSpaceDecorator
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView

@BindingAdapter("itemDecorationSpacing")
fun RecyclerView.setItemDecorationSpacing(
    spacingPx: Int,
    // have added this param because in some case i need to have different space hor and ver
    verticalSpace: Int = spacingPx,
) {
    addItemDecoration(
        RecyclerViewSpaceItemDecoration(spacingPx, verticalSpace),
    )
}

@BindingAdapter("itemDecorationSpacing")
fun RecyclerView.setVerticalItemDecorationSpacing(verticalSpace: Int) {
    addItemDecoration(
        RecyclerViewSpaceItemDecoration(0, verticalSpace),
    )
}

@BindingAdapter("horizontalSpacePx", "topBottomSpacePx", requireAll = false)
fun RecyclerView.setHorizontalItemDecorationSpacing(
    horizontalSpacePx: Int,
    topBottomSpacePx: Int = 0,
) {
    addItemDecoration(
        RecyclerViewHorizontalSpaceItemDecoration(horizontalSpacePx, topBottomSpacePx),
    )
}

@BindingAdapter("dividerDecorator", "dividerStep", "extraPaddingVertical", "extraPaddingHorizontal", requireAll = false)
fun RecyclerView.addDividerDecorator(
    @DrawableRes divider: Int,
    step: Int = 1,
    extraPaddingVertical: Int = 0,
    extraPaddingHorizontal: Int = 0,
) {
    addItemDecoration(DividerItemDecorator(ContextCompat.getDrawable(context, divider), step, extraPaddingVertical, extraPaddingHorizontal))
}

@BindingAdapter("extraSpacingPx")
fun RecyclerView.addExtraSpaceAtStartAndEnd(extraSpacingPx: Int) {
    addItemDecoration(StartEndExtraSpaceDecorator(extraSpacingPx))
}

fun RecyclerView.removeAllDecorations() {
    while (itemDecorationCount > 0)
        removeItemDecorationAt(0)
}
