package com.example.invade.commons.ui.extensions

import com.example.invade.core.utils.DetachableObserver
import androidx.lifecycle.SavedStateHandle
import kotlinx.coroutines.flow.filterNotNull

fun <T : Any> SavedStateHandle.stateFlow(key: String) = getStateFlow<T?>(key, null).filterNotNull()

fun <T : Any> SavedStateHandle.liveData(
    key: String,
    onChange: (T) -> Unit,
) = getLiveData<T>(key).let { liveData ->
    DetachableObserver(liveData, onChange).also {
        liveData.observeForever(it)
    }
}
