package com.example.invade.commons.ui.recyclerview

import com.example.invade.commons.ui.extensions.isAr
import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class RecyclerViewHorizontalSpaceItemDecoration(
    private val horizontalSpacePx: Int,
    private val topBottomSpacePx: Int,
) : RecyclerView.ItemDecoration() {
    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State,
    ) {
        val pos = parent.getChildAdapterPosition(view)
        if (pos == RecyclerView.NO_POSITION) {
            return
        }

        outRect.top = topBottomSpacePx
        outRect.bottom = topBottomSpacePx

        val isAr = view.context.isAr
        val space = horizontalSpacePx / 2
        when (pos) {
            0 -> {
                if (isAr) {
                    outRect.left = space
                } else {
                    outRect.right = space
                }
            }
            state.itemCount - 1 -> {
                if (isAr) {
                    outRect.right = space
                } else {
                    outRect.left = space
                }
            }
            else -> {
                outRect.left = space
                outRect.right = space
            }
        }
    }
}
