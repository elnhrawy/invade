include(
    ":app",
    ":core",
    ":commons:ui",
    ":commons:views",
    ":commons:navigation",
    ":features:splash",
    ":features:home",
    ":features:details",
)
rootProject.name = "Invade"
rootProject.buildFileName = "build.gradle.kts"
