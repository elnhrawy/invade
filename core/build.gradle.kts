import dependencies.AnnotationProcessorsDependencies
import dependencies.Dependencies
import ext.addBoolean
import ext.addInt
import ext.addString
import ext.api
import ext.implementation
import ext.kapt

plugins {
    id("common.android-library")
}

android {
    productFlavors.forEach {
        it.addString(
            "BASE_URL",
            when (Flavor.flavor(it.name)) {
                Flavor.Development -> "http://universities.hipolabs.com/"
            },
        )

    }

    buildTypes.forEach {
        it.addBoolean("DATABASE_EXPORT_SCHEMA", true)
        it.addString("DATABASE_NAME", "invade-db")
        it.addInt("DATABASE_VERSION", 1)
        it.addString("APP_VERSION_NAME", BuildAndroidConfig.VERSION_NAME)
        it.addString("KEY_VALUE_DATABASE_NAME", "key-value-db")
    }

    defaultConfig {
        kapt {
            arguments { arg("room.schemaLocation", "$projectDir/dbSchemas") }
        }

        // this is needed to use Java 8 time library
        multiDexEnabled = true
    }

    sourceSets {
        getByName("test") {
            assets.srcDirs("$projectDir/dbSchemas")
        }
    }

    // this is needed to use Java 8 time library
    compileOptions {
        isCoreLibraryDesugaringEnabled = true
        sourceCompatibility = JavaVersion.VERSION_11
        targetCompatibility = JavaVersion.VERSION_11
    }
    namespace = "com.example.invade.core"
    buildFeatures {
        buildConfig = true
    }
}

dependencies {
    api(Dependencies.ROOM)
    api(Dependencies.ROOM_PAGING)
    implementation(Dependencies.ROOM_KTX)
    implementation(Dependencies.LIFECYCLE_LIVE_DATA)
    implementation(Dependencies.CORE_KTX)
    implementation(Dependencies.RETROFIT)
    implementation(Dependencies.RETROFIT_CONVERTER)
    api(Dependencies.MOSHI)
    implementation(Dependencies.MOSHI_ADAPTERS)
    implementation(Dependencies.OKHTTP)
    implementation(Dependencies.LOGGING)

    kapt(AnnotationProcessorsDependencies.ROOM)
    kapt(AnnotationProcessorsDependencies.MOSHI)

    coreLibraryDesugaring(Dependencies.DESUGAR_JDK) // this is needed to use Java 8 time library
}
