package com.example.invade.core.di.modules

import com.example.invade.core.di.scopes.AppScope
import com.example.invade.core.repos.home.HomeRepo
import com.example.invade.core.repos.home.HomeRepoImpl
import dagger.Binds
import dagger.Module

@Module
abstract class RepositoryModule {


    @AppScope
    @Binds
    abstract fun bindHomeLandingRepo(repo: HomeRepoImpl): HomeRepo


}
