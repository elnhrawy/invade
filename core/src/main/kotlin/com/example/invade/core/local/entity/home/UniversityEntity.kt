package com.example.invade.core.local.entity.home

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "university")
data class UniversityEntity(
    @PrimaryKey(autoGenerate = true) val id: Long = 0L,
    @ColumnInfo(name = "alpha_two_code") val code: String?,
    @ColumnInfo(name = "country") val country: String?,
    @ColumnInfo(name = "name") val name: String?,
    @ColumnInfo(name = "state-province") val state: String?,
    @ColumnInfo(name = "web_pages") val website: String?,

)
