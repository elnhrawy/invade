package com.example.invade.core.mappers

import com.example.invade.core.local.entity.home.UniversityEntity
import com.example.invade.core.remote.response.UniversityResponse

fun UniversityResponse.toLocal() =
    UniversityEntity(
        name = name,
        code = code,
        country = country,
        state = state,
        website = website.takeIf { it?.isNotEmpty() ?: false}?.first()
    )


