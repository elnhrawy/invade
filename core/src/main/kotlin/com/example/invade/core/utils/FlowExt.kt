package com.example.invade.core.utils

import com.example.invade.core.remote.Result

import androidx.lifecycle.LiveData
import androidx.lifecycle.asLiveData
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.transform

fun <T : Any> flowOF(suspendFun: suspend () -> Result<T>): Flow<Result<T>> =
    flow {
        emit(Result.loading())
        emit(suspendFun())
    }

fun <T : Any> Flow<T>.toLiveData(): LiveData<T> = this.asLiveData()

suspend fun <T : Any> MutableStateFlow<T?>.setAndClear(
    value: T,
    idle: T? = null,
    delay: Long = 100,
) {
    this.value = value
    delay(delay)
    this.value = idle
}



@Suppress("FunctionName")
fun <T> CustomMutableSharedFlow(
    replayCount: Int = 1,
    debounceMilliSec: Long? = null,
    dropOldest: Boolean = false,
    onStartValue: (() -> T)? = null,
): SharedFlowAsMutable<T> {
    val mutableSharedFlow =
        when (dropOldest) {
            true -> MutableSharedFlow<T>(replay = replayCount, extraBufferCapacity = 1, onBufferOverflow = BufferOverflow.DROP_OLDEST)
            false -> MutableSharedFlow<T>(replay = replayCount)
        }
    val debounceFlow = debounceMilliSec?.let { mutableSharedFlow.debounce(timeoutMillis = it) } ?: mutableSharedFlow
    val flow = onStartValue?.let { debounceFlow.onStart { emit(it()) } } ?: debounceFlow

    return SharedFlowAsMutable(
        mutableSharedFlow = mutableSharedFlow,
        sharedFlow = flow,
    )
}

class SharedFlowAsMutable<T>(
    private val mutableSharedFlow: MutableSharedFlow<T>,
    private val sharedFlow: Flow<T>,
) : Flow<T> by sharedFlow {
    suspend fun emit(value: T) {
        mutableSharedFlow.emit(value)
    }
}

infix fun <T : Any> Flow<Result<T>>.onSuccess(action: suspend (T) -> Unit): Flow<Result<T>> =
    transform { value ->
        if (value is Result.Success) {
            action(value.data)
        }
        return@transform emit(value)
    }
