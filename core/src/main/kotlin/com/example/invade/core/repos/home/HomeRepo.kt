package com.example.invade.core.repos.home

import androidx.lifecycle.LiveData
import com.example.invade.core.local.entity.home.UniversityEntity
import com.example.invade.core.remote.Result

interface HomeRepo {
    fun getHome(): LiveData<Result<List<UniversityEntity>>>
    fun getUchUniversityById(id: Long): LiveData<UniversityEntity>
}
