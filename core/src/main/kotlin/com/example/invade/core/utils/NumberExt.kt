package com.example.invade.core.utils

fun Int.takeIfGreaterThanZero(): Int? = takeIf { it > 0 }

fun Double.takeIfGreaterThanZero(): Double? = takeIf { it > 0.0 }
