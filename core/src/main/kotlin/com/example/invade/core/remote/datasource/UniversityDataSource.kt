package com.example.invade.core.remote.datasource

import com.example.invade.core.local.entity.SingleEvent
import com.example.invade.core.remote.service.HomeService
import com.squareup.moshi.Moshi
import kotlinx.coroutines.flow.MutableStateFlow

class UniversityDataSource(
    private val apiServices: HomeService,
    moshi: Moshi,
    tokenExpirationFlow: MutableStateFlow<SingleEvent<Long>>,
) : BaseDataSource(moshi, tokenExpirationFlow) {
    suspend fun getHome() =
        getResult {
            apiServices.getHome(COUNTRY)
        }

    companion object{
        const val COUNTRY="United Arab Emirates"
    }
}
