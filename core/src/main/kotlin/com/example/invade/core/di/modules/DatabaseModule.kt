package com.example.invade.core.di.modules

import com.example.invade.core.BuildConfig
import com.example.invade.core.di.scopes.AppScope
import com.example.invade.core.local.database.DariDatabase
import com.example.invade.core.local.keyvalue.KeyValueDatabase
import android.content.Context
import androidx.room.Room
import androidx.room.migration.Migration
import dagger.Module
import dagger.Provides

@Module
class DatabaseModule {
    @Provides
    @AppScope
    fun provideDariDatabase(
        @AppScope context: Context,
    ): DariDatabase {
        val migrations = emptyArray<Migration>()


        return Room.databaseBuilder(
            context,
            DariDatabase::class.java,
            BuildConfig.DATABASE_NAME,
        ).addMigrations(*migrations)
            .fallbackToDestructiveMigration()
            .build()
    }


    @Provides
    @AppScope
    fun provideKeyValueDatabase(
        @AppScope context: Context,
    ) = KeyValueDatabase(context.getSharedPreferences(BuildConfig.KEY_VALUE_DATABASE_NAME, Context.MODE_PRIVATE))



    @Provides
    @AppScope
    fun provideUniversityDao(database: DariDatabase) = database.universityDao()
}
