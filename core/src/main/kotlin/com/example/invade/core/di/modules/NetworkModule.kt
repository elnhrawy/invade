package com.example.invade.core.di.modules

import com.example.invade.core.BuildConfig
import com.example.invade.core.di.scopes.AppScope
import com.example.invade.core.local.entity.SingleEvent
import com.example.invade.core.remote.datasource.UniversityDataSource
import com.example.invade.core.remote.interceptors.NetworkConnectionInterceptor
import com.example.invade.core.remote.service.HomeService
import com.example.invade.core.utils.getMoshiObject
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.filterNot
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit

@Module
class NetworkModule {
    companion object {
        private const val TIME_OUT = 60L
    }

    @AppScope
    @Provides
    fun provideTokenExpirationMutableFlow(): MutableStateFlow<SingleEvent<Long>> = MutableStateFlow(SingleEvent(-1L, -1))

    @AppScope
    @Provides
    fun provideTokenExpirationFlow(tokenExpirationFlow: MutableStateFlow<SingleEvent<Long>>): Flow<SingleEvent<Long>> =
        tokenExpirationFlow.filterNot { it.data == -1L }

    @AppScope
    @Provides
    fun provideHttpLoggingInterceptor() =
        HttpLoggingInterceptor().also {
            it.level = HttpLoggingInterceptor.Level.BODY
        }

    @AppScope
    @Provides
    fun provideOKHttpClient(
        loggingInterceptor: HttpLoggingInterceptor,
//        authHeaderInterceptor: AuthHeaderInterceptor,
        internetConnectionInterceptor: NetworkConnectionInterceptor,
    ): OkHttpClient {
        val httpClientBuilder =
            OkHttpClient.Builder().also {
                // don't remove this ..keep it as they want
                it.writeTimeout(TIME_OUT, TimeUnit.SECONDS)
                    .readTimeout(TIME_OUT, TimeUnit.SECONDS)
                    .callTimeout(TIME_OUT, TimeUnit.SECONDS)
                    .connectTimeout(TIME_OUT, TimeUnit.SECONDS)
//                it.addInterceptor(authHeaderInterceptor)
                if (BuildConfig.DEBUG) {
                    it.addInterceptor(loggingInterceptor)
                }
                it.addInterceptor(internetConnectionInterceptor)
            }
        return httpClientBuilder.build()
    }

    @AppScope
    @Provides
    fun provideMoshi(): Moshi = getMoshiObject()

    @AppScope
    @Provides
    fun provideMoshiConverterFactory(moshi: Moshi): Converter.Factory = MoshiConverterFactory.create(moshi)

    @AppScope
    @Provides
    fun provideRetrofit(
        client: OkHttpClient,
        factory: Converter.Factory,
    ): Retrofit =
        Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .addConverterFactory(factory)
            .client(client)
            .build()

    // =========================== SERVICES =================================

    @AppScope
    @Provides
    fun provideHomeLandingDataSource(
        homeService: HomeService,
        moshi: Moshi,
        tokenExpirationFlow: MutableStateFlow<SingleEvent<Long>>,
    ) = UniversityDataSource(homeService, moshi, tokenExpirationFlow)

    // =========================== DATA_SOURCES =================================

    @AppScope
    @Provides
    fun provideHomeService(retrofit: Retrofit): HomeService = retrofit.create(HomeService::class.java)


}
