package com.example.invade.core.remote.service


import com.example.invade.core.remote.response.UniversityResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface HomeService {
    @GET("search")
    suspend fun getHome(
        @Query("country") country: String,

        ): Response<List<UniversityResponse>>

}
