package com.example.invade.core.local.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.invade.core.BuildConfig
import com.example.invade.core.local.convertor.DBTypeConvertor
import com.example.invade.core.local.dao.UniversityDao
import com.example.invade.core.local.entity.home.UniversityEntity

@Database(
    entities = [
        UniversityEntity::class
    ],
    exportSchema = BuildConfig.DATABASE_EXPORT_SCHEMA,
    version = BuildConfig.DATABASE_VERSION,
)
@TypeConverters(DBTypeConvertor::class)
abstract class DariDatabase : RoomDatabase() {


    abstract fun universityDao(): UniversityDao

}
