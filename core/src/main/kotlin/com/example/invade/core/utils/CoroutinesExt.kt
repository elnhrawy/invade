package com.example.invade.core.utils

import android.app.Application
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

fun <T> debounce(
    waitMs: Long,
    coroutineScope: CoroutineScope,
    destinationFunction: (T) -> Unit,
): (T) -> Unit {
    var debounceJob: Job? = null
    return { param: T ->
        debounceJob?.cancel()
        debounceJob =
            coroutineScope.launch {
                delay(waitMs)
                destinationFunction(param)
            }
    }
}

val Application.coroutineScope: CoroutineScope
    get() = (this as ApplicationScopeProvider).provideScope()
