package com.example.invade.core.local.migrations

object MigrationExceptionHandler {
    fun handleException(onCaught: () -> Unit) {
        val prevHandler = Thread.getDefaultUncaughtExceptionHandler() // reference the crashlytics handler
        Thread.setDefaultUncaughtExceptionHandler { t, e ->
            if (isMigrationException(e, "Migration didn't properly handle")) {
                onCaught()
            } else {
                prevHandler?.uncaughtException(t, e)
            }
        }
    }

    private fun isMigrationException(
        e: Throwable,
        msg: String,
    ): Boolean {
        val cause: Throwable? = e.cause
        val errorMsg = e.message

        return (errorMsg != null && errorMsg.contains(msg, ignoreCase = true)) || (cause != null && isMigrationException(cause, msg))
    }
}
