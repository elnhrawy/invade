package com.example.invade.core.local.keyvalue

import android.content.SharedPreferences

class KeyValueDatabase(private val sharedPreferences: SharedPreferences) {
    var isFirstTimeAppOpened: Boolean
        set(value) {
            with(sharedPreferences.edit()) {
                putBoolean(FIRST_TIME_OPEN_APP, value)
                apply()
            }
        }
        get() = sharedPreferences.getBoolean(FIRST_TIME_OPEN_APP, true)

    var isFirstTimeLoggedIn: Boolean
        set(value) {
            with(sharedPreferences.edit()) {
                putBoolean(FIRST_TIME_LOGIN, value)
                apply()
            }
        }
        get() = sharedPreferences.getBoolean(FIRST_TIME_LOGIN, true)

    var isLanguageSelected: Boolean
        set(value) {
            with(sharedPreferences.edit()) {
                putBoolean(LANGUAGE_SELECTED, value)
                apply()
            }
        }
        get() = sharedPreferences.getBoolean(LANGUAGE_SELECTED, false)

    var appLanguage: String?
        set(value) {
            with(sharedPreferences.edit()) {
                putString(APP_LANGUAGE, value)
                apply()
            }
        }
        get() = sharedPreferences.getString(APP_LANGUAGE, null)

    var forceUpdate: Boolean
        set(value) {
            with(sharedPreferences.edit()) {
                putBoolean(FORCE_UPDATE, value)
                commit()
            }
        }
        get() = sharedPreferences.getBoolean(FORCE_UPDATE, false)

    var marketAppVersion: String?
        set(value) {
            with(sharedPreferences.edit()) {
                putString(MARKET_APP_VERSION, value)
                commit()
            }
        }
        get() = sharedPreferences.getString(MARKET_APP_VERSION, null)

    var lastUpdateAppTime: Long
        set(value) {
            with(sharedPreferences.edit()) {
                putLong(LAST_UPDATE_APP, value)
                apply()
            }
        }
        get() = sharedPreferences.getLong(LAST_UPDATE_APP, -1)

    val isEnglish: Boolean
        get() = appLanguage.contentEquals(LANG_ENGLISH)

    companion object {
        const val LANG_ENGLISH = "en"
        const val LANG_ARABIC = "ar"

        private const val FIRST_TIME_OPEN_APP = "first_time"
        private const val FIRST_TIME_LOGIN = "first_time_login"
        private const val LANGUAGE_SELECTED = "lang_selected"
        private const val APP_LANGUAGE = "app_language"
        private const val FORCE_UPDATE = "force_update"
        private const val MARKET_APP_VERSION = "market_app_version"
        private const val LAST_UPDATE_APP = "last_update_time"
    }
}
