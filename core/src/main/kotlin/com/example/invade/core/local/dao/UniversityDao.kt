package com.example.invade.core.local.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import com.example.invade.core.local.entity.home.UniversityEntity

@Dao
interface UniversityDao {
    @Query("SELECT * FROM university")
    suspend fun getAllUniversitiesList(): List<UniversityEntity>

    @Query("SELECT * FROM university")
    fun getAllUniversitiesLiveData(): LiveData<List<UniversityEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertUniversity(universityEntity: UniversityEntity): Long

    @Query("DELETE FROM university")
    suspend fun deleteAll()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(universityEntityList: List<UniversityEntity>)

    @Query("SELECT * FROM university WHERE id = :id")
    suspend fun getUniversityById(id: Long): UniversityEntity?

    @Transaction
    @Query("SELECT * FROM university WHERE id = :id LIMIT 1")
    fun getUniversityByIdLiveData(id: Long): LiveData<UniversityEntity?>


}
