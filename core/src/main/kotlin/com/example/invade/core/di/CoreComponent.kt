package com.example.invade.core.di

import android.app.Application
import android.content.Context
import com.example.invade.core.di.modules.ContextModule
import com.example.invade.core.di.modules.DatabaseModule
import com.example.invade.core.di.modules.NetworkModule
import com.example.invade.core.di.modules.RepositoryModule
import com.example.invade.core.di.scopes.AppScope
import com.example.invade.core.local.database.DariDatabase
import com.example.invade.core.local.entity.SingleEvent
import com.example.invade.core.repos.home.HomeRepo
import dagger.Component
import kotlinx.coroutines.flow.Flow

@AppScope
@Component(
    modules = [
        ContextModule::class,
        DatabaseModule::class,
        NetworkModule::class,
        RepositoryModule::class,
    ],
)
interface CoreComponent {
    fun context(): Context

    fun application(): Application

    fun homeLandingRepo(): HomeRepo

    fun tokenExpirationFlow(): Flow<SingleEvent<Long>>

    fun dariDatabase(): DariDatabase

}
