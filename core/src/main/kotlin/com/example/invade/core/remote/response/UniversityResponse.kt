package com.example.invade.core.remote.response

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass


@JsonClass(generateAdapter = true)
data class UniversityResponse(
    @field:Json(name = "alpha_two_code") val code: String?,
    @field:Json(name = "country") val country: String?,
    @field:Json(name = "name") val name: String?,
    @field:Json(name = "state-province") val state: String?,
    @field:Json(name = "web_pages") val website: List<String>?,
)


