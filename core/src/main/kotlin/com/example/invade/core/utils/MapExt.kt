package com.example.invade.core.utils

fun <T> Map<String, Any?>.get(key: String): T? = this[key] as? T

fun <T> Map<String, List<Any>?>.getSingle(key: String): T? = this[key]?.firstOrNull() as? T

fun <T> Map<String, List<Any>?>.getAs(key: String): List<T>? = this[key] as? List<T>

fun <T> Map<String, Any?>.getNotDeletedDataList(key: String): List<T>? =
    (this[key] as? List<Pair<T, Boolean>>)?.filter { it.second }
        ?.map { it.first }

fun <K, V> Map<K, V?>.filterNotNullValues(): Map<K, V> = mutableMapOf<K, V>().apply { for ((k, v) in this@filterNotNullValues) if (v != null) put(k, v) }
