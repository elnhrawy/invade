package com.example.invade.core.utils

import java.lang.System.nanoTime
import kotlin.random.Random

val random = Random(nanoTime())
const val MIN_RANDOM_NUMBER = 10000

fun randomID(length: Int): String {
    val allowedChars = ('A'..'Z') + ('a'..'z') + ('0'..'9')
    return (1..length)
        .map { allowedChars.random() }
        .joinToString("")
}

fun randomNumber(): Int = random.nextInt(MIN_RANDOM_NUMBER, Int.MAX_VALUE)
