package com.example.invade.core.utils

import android.content.Context
import android.net.Uri
import android.provider.MediaStore
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.File
import java.io.FileOutputStream

fun Uri.getDisplayName(context: Context): String {
    val cursor =
        context.contentResolver.query(
            this,
            arrayOf(MediaStore.MediaColumns.DISPLAY_NAME),
            null,
            null,
            null,
        )

    return try {
        if (cursor != null) {
            cursor.moveToFirst()
            cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DISPLAY_NAME)).also {
                cursor.close()
            }
        } else {
            ""
        }
    } catch (_: Throwable) {
        ""
    }
}

suspend fun Uri.toFile(context: Context): File? =
    withContext(Dispatchers.IO) {
        return@withContext try {
            context.contentResolver?.openInputStream(this@toFile)?.use { input ->
                val fileName = this@toFile.getDisplayName(context)
                val filePath = "${context.cacheDir}${File.separator}$fileName"
                val file = File(filePath)
                FileOutputStream(file).use { output ->
                    val buffer = ByteArray(size = 4096)
                    do {
                        val len: Int =
                            input.read(buffer).takeIf { it > 0 }?.also {
                                output.write(buffer, 0, it)
                            } ?: 0
                    } while (len > 0)
                }
                file
            }
        } catch (_: Throwable) {
            null
        }
    }
