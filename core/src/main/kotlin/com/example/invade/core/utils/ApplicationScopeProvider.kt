package com.example.invade.core.utils

import kotlinx.coroutines.CoroutineScope

interface ApplicationScopeProvider {
    fun provideScope(): CoroutineScope
}
