package com.example.invade.core.repos.home

import com.example.invade.core.local.dao.UniversityDao
import com.example.invade.core.local.entity.home.UniversityEntity
import com.example.invade.core.mappers.toLocal
import com.example.invade.core.remote.datasource.UniversityDataSource
import com.example.invade.core.remote.response.UniversityResponse
import com.example.invade.core.utils.resultLiveData
import com.example.invade.core.remote.Result
import com.example.invade.core.utils.first
import com.example.invade.core.utils.networkOnlyLiveData
import com.example.invade.core.utils.randomID
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import androidx.lifecycle.LiveData
import com.example.invade.core.utils.filterNotNull

import javax.inject.Inject



class HomeRepoImpl
@Inject
constructor(
    private val remote: UniversityDataSource,
    private val universityDao: UniversityDao,
) : HomeRepo {

    private suspend fun saveHomeResponseToLocal(homeResponse: List<UniversityResponse>) {
        universityDao.deleteAll()
        homeResponse.let { universityDao.insertAll(it.map { it.toLocal() }) }
    }

    override fun getHome(): LiveData<Result<List<UniversityEntity>>> =
        resultLiveData(
            localCall = {
                universityDao.getAllUniversitiesLiveData()
            },
            networkCall = {
                remote.getHome()
            },
            saveCallResult = {
                saveHomeResponseToLocal(it)
            },
        )

    override fun getUchUniversityById(id: Long): LiveData<UniversityEntity> =
        universityDao.getUniversityByIdLiveData(id).filterNotNull()



}
