package com.example.invade.core.utils

interface CoreComponentProvider {
    fun provideCoreComponent(): com.example.invade.core.di.CoreComponent
}
