package com.example.invade.core.utils

@Suppress("SwallowedException")
class MutableMapDelegate(val multiple: MutableMap<String, MutableList<Pair<Any, Boolean>>>, val single: MutableMap<String, Any?>) : HashMap<String, Any?>() {
    override fun put(
        key: String,
        value: Any?,
    ): Any? {
        val oldValue = get(key)
        when (value) {
            null ->
                if (single.containsKey(key)) {
                    single[key] = null
                } else if (multiple.containsKey(key)) {
                    multiple[key] = mutableListOf()
                }

            is MutableList<*> -> {
                multiple[key] = value as MutableList<Pair<Any, Boolean>>
            }

            else ->
                single[key] = value
        }
        return oldValue
    }

    override fun get(key: String): Any? {
        return arrayOf(multiple, single).firstOrNull { it.containsKey(key) }?.get(key)
    }
}
