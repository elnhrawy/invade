package com.example.invade.core.utils

fun Boolean?.isNullORFalse(): Boolean = this == null || this == false
