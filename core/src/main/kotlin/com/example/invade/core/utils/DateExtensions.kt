package com.example.invade.core.utils

import java.sql.Time
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.LocalTime
import java.time.Period
import java.time.temporal.ChronoUnit
import java.util.Calendar
import java.util.Date
import java.util.Locale
import java.util.concurrent.TimeUnit
import kotlin.math.max
import kotlin.math.min

infix fun Date.duration(other: Date?): Duration {
    if (other == null) return Duration()

    val (from, to) = getLocalDates(this, other)
    val period = Period.between(from, to)

    return Duration(period.years, period.months, period.days)
}

infix fun Date.durationDays(other: Date?): Int {
    if (other == null) return -1

    val (from, to) = getLocalDates(this, other)

    return ChronoUnit.DAYS.between(from, to).toInt()
}

private fun getLocalDates(
    from: Date,
    to: Date,
): Pair<LocalDate, LocalDate> {
    val fromCalendar = Calendar.getInstance().also { it.timeInMillis = min(from.time, to.time) }
    val toCalendar = Calendar.getInstance().also { it.timeInMillis = max(from.time, to.time) }

    return LocalDate.ofYearDay(fromCalendar.get(Calendar.YEAR), fromCalendar.get(Calendar.DAY_OF_YEAR)) to
        LocalDate.ofYearDay(toCalendar.get(Calendar.YEAR), toCalendar.get(Calendar.DAY_OF_YEAR))
}

fun Date.toCalendar(): Calendar =
    Calendar.getInstance().also {
        it.time = this
    }

fun Calendar.getDay() = get(Calendar.DAY_OF_YEAR)

fun Calendar.getMonth() = get(Calendar.MONTH)

data class Duration(
    val year: Int = 0,
    val months: Int = 0,
    val days: Int = 0,
)

fun Calendar.setMidnight(): Calendar {
    set(Calendar.HOUR_OF_DAY, 0)
    set(Calendar.MINUTE, 0)
    set(Calendar.SECOND, 0)
    set(Calendar.MILLISECOND, 0)
    return this
}

fun Long.getDateDiffToToday(): Long {
    return try {
        TimeUnit.DAYS.convert(System.currentTimeMillis() - this, TimeUnit.MILLISECONDS)
    } catch (_: Exception) {
        0
    }
}

fun Date.plusYears(years: Int): Date {
    val calendar = Calendar.getInstance()
    calendar.time = this
    calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR) + years)
    return Date(calendar.timeInMillis)
}

private const val DAY_MILLI = 86400000L

fun Date.addDays(days: Int) = Date(time + (days * DAY_MILLI))

fun Date.addMonths(months: Int): Date {
    val calendar = Calendar.getInstance()
    calendar.time = this
    calendar.add(Calendar.MONTH, months)
    return calendar.time
}
fun Date.addYears(years: Int): Date {
    val calendar = Calendar.getInstance()
    calendar.time = this
    calendar.add(Calendar.YEAR, years)
    return calendar.time
}

fun Date.subDays(days: Int) = Date(time - (days * DAY_MILLI))

fun SimpleDateFormat.formatNullable(date: Date?): String = date?.let { this.format(date) } ?: ""

fun Long.toUITimeFormat(): String {
    val sdf = SimpleDateFormat("hh:mm a", Locale.US)
    val calender = Calendar.getInstance()
    calender.timeInMillis = this
    return sdf.format(calender.time)
}

fun Long.toBETimeFormat(): String {
    val sdf = SimpleDateFormat("HH:mm:ss", Locale.US)
    val calender = Calendar.getInstance()
    calender.timeInMillis = this
    return sdf.format(calender.time)
}

fun String.toUITimeFormat(): String {
    val sdf = SimpleDateFormat("hh:mm a", Locale.US)
    val calender = Calendar.getInstance()
    calender.timeInMillis = Time.valueOf(this).time
    return sdf.format(calender.time)
}

fun String.toLongTimeFormat(): Long {
    val localTime = LocalTime.parse(this)
    return (localTime.toSecondOfDay() * 1000).toLong()
}
