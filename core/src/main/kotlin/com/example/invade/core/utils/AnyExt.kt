package com.example.invade.core.utils

import java.security.MessageDigest

fun Any.hashCodeString(): String = MessageDigest.getInstance("SHA-1").digest(toString().toByteArray()).toHex()
