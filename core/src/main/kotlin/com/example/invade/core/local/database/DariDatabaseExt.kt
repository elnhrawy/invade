package com.example.invade.core.local.database

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

suspend fun DariDatabase.delete() =
    withContext(Dispatchers.IO) {
        clearAllTables()
    }
