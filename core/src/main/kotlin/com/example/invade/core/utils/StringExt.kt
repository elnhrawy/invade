package com.example.invade.core.utils

import android.util.Base64
import okhttp3.MultipartBody
import okhttp3.RequestBody

const val ARABIC_LETTERS_REGEX = "^[\u0600-\u065F\u066A-\u06EF\u06FA-\u06FF]+\$"
const val ENGLISH_LETTERS_REGEX = "^[A-Za-z]*$"
const val STR_DELIMITER = "§"
const val PASSPORT_REGEX = "^[a-zA-Z]{1,3}\\d{5,7}$" // doesn't cover all cases
const val EID_REGEX = "^(\\d{15})|(\\d{3}-\\d{4}-\\d{7}-\\d{1})\$"
const val COMPANY_TRADE_NUMBER_LICENSE_REGEX = "^(TN-[\\d-]*)|(CN-[\\d-]*)|(B.L. [\\d]{1,3}\\/[\\d]{1,2})|(?:[0-9]{1,3}\\.){3}[0-9]{1,3}\$\$"

/**
 * get file name from file path
 */
fun String.getFileName(): String =
    this.lastIndexOf('/').takeIf { it >= 0 && it + 1 < length }
        ?.let { substring(it + 1) } ?: this

fun String.toBase64(): String = Base64.encodeToString(this.toByteArray(), Base64.NO_WRAP)

fun String.toNumericalTextInput(): String? = toDoubleOrNull()?.toString()

fun String.isOnlyDigits(): Boolean = this.matches("[0-9]+".toRegex())

fun String.isOnlyLetters(): Boolean = this.matches(ARABIC_LETTERS_REGEX.toRegex()) or this.matches(ENGLISH_LETTERS_REGEX.toRegex())

fun String.matches(
    regexString: String,
    ignoreCase: Boolean = false,
) = matches(if (ignoreCase) regexString.toRegex(RegexOption.IGNORE_CASE) else regexString.toRegex())

fun String.contentEqualsNull(): Boolean = this.contentEquals(other = "null", ignoreCase = true)

fun String.takeIfContentNotNull(): String? = this.takeIf { it.contentEqualsNull().not() }

fun String?.orBlank(default: String = "~"): String = if (isNullOrBlank()) default else this

fun String.isSensitiveData(): Boolean =
    matches(EID_REGEX) ||
        matches(COMPANY_TRADE_NUMBER_LICENSE_REGEX) ||
        matches(PASSPORT_REGEX)

fun String?.toPartBody() = this?.let { RequestBody.create(MultipartBody.FORM, it) }
