package com.example.invade.core.utils

import java.io.File

fun File.getMIMEType(): String? =
    name.substringAfterLast(delimiter = ".", missingDelimiterValue = "NaN")
        .getMIMETypeFromFileExtension()

fun String.getMIMETypeFromFileExtension() =
    when (this) {
        "pdf", "PDF" -> "application/pdf"
        "png", "PNG" -> "image/png"
        "jpg", "JPG", "jpeg", "JPEG" -> "image/jpeg"
        else -> null
    }
