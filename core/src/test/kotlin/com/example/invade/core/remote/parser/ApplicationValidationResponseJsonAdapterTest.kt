package com.example.invade.core.remote.parser

import com.example.invade.core.remote.response.ApplicationValidationResponse
import com.example.invade.core.remote.response.MultiUnitValidationError
import com.example.invade.core.remote.response.PropertiesValidationError
import com.example.invade.core.remote.response.PropertiesValidationIssues
import com.example.invade.core.remote.response.SubPMAErrorDetails
import com.example.invade.core.remote.response.ValidationError
import com.example.invade.core.utils.getMoshiObject
import com.squareup.moshi.Moshi
import org.junit.Assert.assertTrue
import org.junit.Test

class ApplicationValidationResponseJsonAdapterTest {
    private val defaultJsonAdapter = Moshi.Builder().build().adapter(ApplicationValidationResponse::class.java)
    private val mainJsonAdapter = getMoshiObject().adapter(ApplicationValidationResponse::class.java)

    @Test
    fun fromJson_parse_no_errors() {
        val validationResponse =
            ApplicationValidationResponse(
                validationErrors = listOf(),
                pmaOwnershipChanged = null,
                subPMASExist = null,
                multiUnitsValidationErrors = null,
            )
        val json = defaultJsonAdapter.toJson(validationResponse)
        val result = mainJsonAdapter.fromJson(json)
        assertTrue(result != null)
        assertTrue(result?.validationErrors.isNullOrEmpty())
        assertTrue(result?.multiUnitsValidationErrors.isNullOrEmpty())
        assertTrue(result?.pmaOwnershipChanged.isNullOrEmpty())
        assertTrue(result?.subPMASExist.isNullOrEmpty())
    }

    @Test
    fun fromJson_parse_validationErrors() {
        val validationResponse =
            ApplicationValidationResponse(
                validationErrors = listOf(ValidationError(failureCode = "val123")),
                pmaOwnershipChanged = null,
                subPMASExist = null,
                multiUnitsValidationErrors = null,
            )
        val json = defaultJsonAdapter.toJson(validationResponse)

        val result = mainJsonAdapter.fromJson(json)
        assert(result != null)
        assert(result?.validationErrors.isNullOrEmpty().not())
    }

    @Test
    fun fromJson_parse_pmaOwnershipChanged() {
        val validationResponse =
            ApplicationValidationResponse(
                validationErrors = null,
                multiUnitsValidationErrors = null,
                pmaOwnershipChanged = null,
                subPMASExist = listOf(SubPMAErrorDetails(failureCode = "val789", null)),
            )
        val json = defaultJsonAdapter.toJson(validationResponse)

        val result = mainJsonAdapter.fromJson(json)
        assert(result != null)
        assert(result?.subPMASExist.isNullOrEmpty().not())
    }

    @Test
    fun fromJson_parse_subPMASExist() {
        val validationResponse =
            ApplicationValidationResponse(
                validationErrors = null,
                multiUnitsValidationErrors = null,
                pmaOwnershipChanged =
                    listOf(
                        PropertiesValidationError(failureCode = "val456", null),
                    ),
                subPMASExist = null,
            )
        val json = defaultJsonAdapter.toJson(validationResponse)

        val result = mainJsonAdapter.fromJson(json)
        assert(result != null)
        assert(result?.pmaOwnershipChanged.isNullOrEmpty().not())
    }

    @Test
    fun fromJson_parse_all() {
        val validationResponse =
            ApplicationValidationResponse(
                validationErrors = listOf(ValidationError(failureCode = "val123")),
                multiUnitsValidationErrors =
                    listOf(
                        MultiUnitValidationError(
                            failureCode = "val663",
                            propertiesDetails =
                                listOf(
                                    PropertiesValidationIssues(
                                        plotID = 100L,
                                        applicationNumbers = listOf("13133"),
                                    ),
                                ),
                        ),
                    ),
                pmaOwnershipChanged =
                    listOf(
                        PropertiesValidationError(failureCode = "val456", null),
                    ),
                subPMASExist = listOf(SubPMAErrorDetails(failureCode = "val789", null)),
            )
        val json = defaultJsonAdapter.toJson(validationResponse)

        val result = mainJsonAdapter.fromJson(json)
        assert(result != null)
        assert(result?.validationErrors.isNullOrEmpty().not())
        assert(result?.multiUnitsValidationErrors.isNullOrEmpty().not())
        assert(result?.pmaOwnershipChanged.isNullOrEmpty().not())
        assert(result?.subPMASExist.isNullOrEmpty().not())
    }

    @Test
    fun fromJson_parse_unknown_error_should_throw_exception() {
        val json = "{\"PMA_CONTRACT_UNITS_OWNERSHIP_CHANGE\":[{\"failureCode\":\"val456\"}],\"x_error\": [{\"error_code\":\"val123\"}]}"
        val result = mainJsonAdapter.fromJson(json)
        assert(result?.validationErrors?.any { it.failureCode == "VAL001" } == true)
    }
}
