plugins {
    `kotlin-dsl`
}

repositories {
    google()
    mavenCentral()
    gradlePluginPortal()
}

object PluginsVersions {
    const val GRADLE_ANDROID = "8.2.1"
    const val GRADLE_VERSIONS = "0.50.0"
    const val KOTLIN = "1.9.22"
    const val NAVIGATION = "2.7.6"
    const val GRAPH_GENERATOR = "0.8.0"
    const val DOKKA = "1.9.10"
    const val KTLINT = "12.1.0"
    const val SPOTLESS = "6.21.0"
    const val DETEKT = "1.23.4"
    const val GOOGLE_SERVICES = "4.4.0"
    const val CRASHLYTICS_PLUGIN = "2.9.9"
    const val FIREBASE_PERF_MONITOR_PLUGIN = "1.4.2"
    const val DYNATRACE = "8.+"
}

dependencies {
    implementation("com.android.tools.build:gradle:${PluginsVersions.GRADLE_ANDROID}")
    implementation("com.google.firebase:perf-plugin:${PluginsVersions.FIREBASE_PERF_MONITOR_PLUGIN}")
    implementation("org.jetbrains.kotlin:kotlin-gradle-plugin:${PluginsVersions.KOTLIN}")
    implementation("org.jetbrains.kotlin:kotlin-allopen:${PluginsVersions.KOTLIN}")
    implementation("androidx.navigation:navigation-safe-args-gradle-plugin:${PluginsVersions.NAVIGATION}")
    implementation("com.vanniktech:gradle-dependency-graph-generator-plugin:${PluginsVersions.GRAPH_GENERATOR}") // `generateDependencyGraph`

}
