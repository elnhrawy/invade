object BuildAndroidConfig {
    const val APPLICATION_ID = "com.example.invade"

    const val BUILD_TOOLS_VERSION = "34.0.0"
    const val COMPILE_SDK_VERSION = 34
    const val MIN_SDK_VERSION = 26
    const val TARGET_SDK_VERSION = 34

    const val VERSION_CODE = 1
    const val VERSION_NAME = "1.0.0"

    const val SUPPORT_LIBRARY_VECTOR_DRAWABLES = true

    const val IS_RESIZE_ENABLED = "false"

    const val TEST_INSTRUMENTATION_RUNNER = "androidx.test.runner.AndroidJUnitRunner"
    val TEST_INSTRUMENTATION_RUNNER_ARGUMENTS = mapOf(
        "leakcanary.FailTestOnLeakRunListener" to "listener"
    )
}
