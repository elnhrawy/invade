object BuildModules {
    const val APP = ":app"
    const val CORE = ":core"

    object Features {
        const val SPLASH = ":features:splash"
        const val HOME = ":features:home"
        const val DETAILS = ":features:details"
    }

    object Commons {
        const val UI = ":commons:ui" // base ui component
        const val VIEWS = ":commons:views" // shared views
        const val NAVIGATION = ":commons:navigation"
    }


}
