# Introduction
# MODULAR-MVVM-Clean-Architecture
This is a client app for Invade Assessment  & basic code that demonstrate how to build an Android application using the Uncle Bob's Clean Architecture approach.
<img src="https://drive.google.com/file/d/1RmGktARr9AJjbEYRsAnulSkEhht9yMFO/view?usp=sharing" width="500">

From the Android documentation, Modularization is the practice of organizing a codebase into loosely coupled and self-contained parts. Each part is a module. Each module is independent and serves a clear purpose. By dividing a problem into smaller and easier-to-solve subproblems, you reduce the complexity of designing and maintaining a large system.

Some o the benefits of modularization are scalability, Testability, encapsulation, and improved build time. You should consider modularization for your app if you want to enjoy such benefits.

# MVVM-Clean-Architecture:
Model-view-viewmodel is a software design pattern consisting of three layers:

Model (in our architecture combined with Clean Architecture refers to a domain model representing real state content).
View (activities, fragments, it displays data received through view-model).
View Model (the View Model represents an abstraction of the view, it receives data from the Model, performs necessary UI logic, and then exposes appropriate data to the View, alongside that, View Model manipulates the Model based on actions on the View. The View has a reference to a View Model but View Model doesn’t know anything about the View).

### Modules
Modules are the collection of source files and build settings that allow you to divide your project into discrete units of functionality.

- **App Module**

  `:app` module is an [com.android.application](https://developer.android.com/studio/projects/android-library), which is needed to create the app bundle. It contains dependency graph and UI related classes. It presents data to screen and handle user interactions.

- **Common Module**

  `:common` module contains code and resources which are shared between other modules

- **Core Module**

  `:core` module contains bellow packages:

  `:local` module contains local data source related classes

  `:remote` module contains remote data source related classes

  `:di` module contains implementation of repository and local - remote repository interface adapt
  
  `:repos` module contains use cases and repository interface adapt

  `:utils` most of useful extensions.

- **features Module**

  `:features` module contains business logic

Each module has its own test.

### Tech Stack
- [Kotlin](https://kotlinlang.org)
- [Jetpack](https://developer.android.com/jetpack)
    * [Android KTX](https://developer.android.com/kotlin/ktx)
    * [Lifecycle](https://developer.android.com/topic/libraries/architecture/lifecycle)
    * [Data Binding](https://developer.android.com/topic/libraries/data-binding)
    * [View Binding](https://developer.android.com/topic/libraries/view-binding)
    *  [ViewModel](https://developer.android.com/topic/libraries/architecture/viewmodel)
    * [Navigation Component](https://developer.android.com/guide/navigation/navigation-getting-started)
    * [Room](https://developer.android.com/training/data-storage/room)
- [Coroutines - Flow](https://kotlinlang.org/docs/reference/coroutines/flow.html)
    - [State Flow](https://developer.android.com/kotlin/flow/stateflow-and-sharedflow)
    -   [Shared Flow](https://developer.android.com/kotlin/flow/stateflow-and-sharedflow)
    -  [Channels](https://kotlinlang.org/docs/channels.html#channel-basics)
- [Dagger Hilt](https://dagger.dev/hilt/)
- [Retrofit](https://square.github.io/retrofit/)
- [OkHttp](https://github.com/square/okhttp)
- [KotlinX](https://github.com/Kotlin/kotlinx.serialization)
- [KotlinX Serialization Converter](https://github.com/JakeWharton/retrofit2-kotlinx-serialization-converter)
- [LeakCanary](https://square.github.io/leakcanary/)
